import 'package:flutter/material.dart';
import 'package:date_utils/date_utils.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:table_calendar/table_calendar.dart';

// Example holidays
final Map<DateTime, List> _holidays = {
  DateTime(2019, 1, 1): ['New Year\'s Day'],
  DateTime(2019, 1, 6): ['Epiphany'],
  DateTime(2019, 2, 14): ['Valentine\'s Day'],
  DateTime(2019, 4, 21): ['Easter Sunday'],
  DateTime(2019, 4, 22): ['Easter Monday'],
};

void main() {
  initializeDateFormatting().then((_) => runApp(CalendarViewApp()));
}

class CalendarViewApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Calendar Demo',
      theme: ThemeData(
        primarySwatch: Colors.green,
      ),
      home: MyHomePage(title: 'Calendar Demo'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> with TickerProviderStateMixin {
  DateTime _selectedDay;
  Map<DateTime, List> _events;
  Map<DateTime, List> _visibleEvents;
  Map<DateTime, List> _visibleHolidays;
  List _selectedEvents;
  AnimationController _controller;

  @override
  void initState() {
    super.initState();
    _selectedDay = DateTime.now();
    print("weekday is ${_selectedDay.weekday}");
    switch(_selectedDay.weekday)
    {
      case 1:
        _events = {
          _selectedDay.subtract(Duration(days: 27)): ['Event A0', 'Event B0', 'Event C0'],
          _selectedDay.subtract(Duration(days: 20)): ['Event A0', 'Event B0', 'Event C0'],
          _selectedDay.subtract(Duration(days: 13)): ['Event A0', 'Event B0', 'Event C0'],
          _selectedDay.subtract(Duration(days: 6)): ['Event A1'],
          _selectedDay.add(Duration(days: 1)): ['Event A7', 'Event B7', 'Event C7', 'Event D7'],
          _selectedDay.add(Duration(days: 8)): ['Event A8', 'Event B8', 'Event C8', 'Event D8'],
          _selectedDay.add(Duration(days: 15)): ['Event A9', 'Event A9', 'Event B9'],
          _selectedDay.add(Duration(days: 22)): ['Event A9', 'Event A9', 'Event B9'],
        };
        break;
      case 2:
        _events = {
          _selectedDay.subtract(Duration(days: 27)): ['Event A0', 'Event B0', 'Event C0'],
          _selectedDay.subtract(Duration(days: 20)): ['Event A0', 'Event B0', 'Event C0'],
          _selectedDay.subtract(Duration(days: 13)): ['Event A0', 'Event B0', 'Event C0'],
          _selectedDay.subtract(Duration(days: 6)): ['Event A1'],
          _selectedDay.add(Duration(days: 1)): ['Event A7', 'Event B7', 'Event C7', 'Event D7'],
          _selectedDay.add(Duration(days: 8)): ['Event A8', 'Event B8', 'Event C8', 'Event D8'],
          _selectedDay.add(Duration(days: 15)): ['Event A9', 'Event A9', 'Event B9'],
          _selectedDay.add(Duration(days: 22)): ['Event A9', 'Event A9', 'Event B9'],

        };
        break;
      case 3:

        _events = {
          _selectedDay.subtract(Duration(days: 27)): ['Event A0', 'Event B0', 'Event C0'],
          _selectedDay.subtract(Duration(days: 20)): ['Event A0', 'Event B0', 'Event C0'],
          _selectedDay.subtract(Duration(days: 13)): ['Event A0', 'Event B0', 'Event C0'],
          _selectedDay.subtract(Duration(days: 6)): ['Event A1'],
          _selectedDay.add(Duration(days: 1)): ['Event A7', 'Event B7', 'Event C7', 'Event D7'],
          _selectedDay.add(Duration(days: 8)): ['Event A8', 'Event B8', 'Event C8', 'Event D8'],
          _selectedDay.add(Duration(days: 15)): ['Event A9', 'Event A9', 'Event B9'],
          _selectedDay.add(Duration(days: 22)): ['Event A9', 'Event A9', 'Event B9'],
        };
        break;
      case 4:
        _events = {
          _selectedDay.subtract(Duration(days: 27)): ['Event A0', 'Event B0', 'Event C0'],
          _selectedDay.subtract(Duration(days: 20)): ['Event A0', 'Event B0', 'Event C0'],
          _selectedDay.subtract(Duration(days: 13)): ['Event A0', 'Event B0', 'Event C0'],
          _selectedDay.subtract(Duration(days: 6)): ['Event A1'],
          _selectedDay.add(Duration(days: 1)): ['Event A7', 'Event B7', 'Event C7', 'Event D7'],
          _selectedDay.add(Duration(days: 8)): ['Event A8', 'Event B8', 'Event C8', 'Event D8'],
          _selectedDay.add(Duration(days: 15)): ['Event A9', 'Event A9', 'Event B9'],
          _selectedDay.add(Duration(days: 22)): ['Event A9', 'Event A9', 'Event B9'],
        };
        break;
      case 5:
        _events = {
          _selectedDay.subtract(Duration(days: 27)): ['Event A0', 'Event B0', 'Event C0'],
          _selectedDay.subtract(Duration(days: 20)): ['Event A0', 'Event B0', 'Event C0'],
          _selectedDay.subtract(Duration(days: 13)): ['Event A0', 'Event B0', 'Event C0'],
          _selectedDay.subtract(Duration(days: 6)): ['Event A1'],
          _selectedDay.add(Duration(days: 1)): ['Event A7', 'Event B7', 'Event C7', 'Event D7'],
          _selectedDay.add(Duration(days: 8)): ['Event A8', 'Event B8', 'Event C8', 'Event D8'],
          _selectedDay.add(Duration(days: 15)): ['Event A9', 'Event A9', 'Event B9'],
          _selectedDay.add(Duration(days: 22)): ['Event A9', 'Event A9', 'Event B9'],
        };
        break;
      case 6:
        _events = {
          _selectedDay.subtract(Duration(days: 27)): ['Event A0', 'Event B0', 'Event C0'],
          _selectedDay.subtract(Duration(days: 20)): ['Event A0', 'Event B0', 'Event C0'],
          _selectedDay.subtract(Duration(days: 13)): ['Event A0', 'Event B0', 'Event C0'],
          _selectedDay.subtract(Duration(days: 6)): ['Event A1'],
          _selectedDay.add(Duration(days: 1)): ['Event A7', 'Event B7', 'Event C7', 'Event D7'],
          _selectedDay.add(Duration(days: 8)): ['Event A8', 'Event B8', 'Event C8', 'Event D8'],
          _selectedDay.add(Duration(days: 15)): ['Event A9', 'Event A9', 'Event B9'],
          _selectedDay.add(Duration(days: 22)): ['Event A9', 'Event A9', 'Event B9'],
        };
        break;
      case 7:
        _events = {
          _selectedDay.subtract(Duration(days: 27)): ['Event A0', 'Event B0', 'Event C0'],
          _selectedDay.subtract(Duration(days: 20)): ['Event A0', 'Event B0', 'Event C0'],
          _selectedDay.subtract(Duration(days: 13)): ['Event A0', 'Event B0', 'Event C0'],
          _selectedDay.subtract(Duration(days: 6)): ['Event A1'],
          _selectedDay.add(Duration(days: 1)): ['Event A7', 'Event B7', 'Event C7', 'Event D7'],
          _selectedDay.add(Duration(days: 8)): ['Event A8', 'Event B8', 'Event C8', 'Event D8'],
          _selectedDay.add(Duration(days: 15)): ['Event A9', 'Event A9', 'Event B9'],
          _selectedDay.add(Duration(days: 22)): ['Event A9', 'Event A9', 'Event B9'],
        };
        break;
    }

    _selectedEvents = _events[_selectedDay] ?? [];
    _visibleEvents = _events;
    _visibleHolidays = _holidays;

    _controller = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 400),
    );

    _controller.forward();
    }

  void _onDaySelected(DateTime day, List events) {
    setState(() {
      _selectedDay = day;
      _selectedEvents = events;
    });
  }

  void _onVisibleDaysChanged(DateTime first, DateTime last, CalendarFormat format) {
    setState(() {
      _visibleEvents = Map.fromEntries(
        _events.entries.where(
              (entry) =>
          entry.key.isAfter(first.subtract(const Duration(days: 1))) &&
              entry.key.isBefore(last.add(const Duration(days: 1))),
        ),
      );

      _visibleHolidays = Map.fromEntries(
        _holidays.entries.where(
              (entry) =>
          entry.key.isAfter(first.subtract(const Duration(days: 1))) &&
              entry.key.isBefore(last.add(const Duration(days: 1))),
        ),
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Column(
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          // Switch out 2 lines below to play with TableCalendar's settings
          //-----------------------
          //_buildTableCalendar(),
          _buildTableCalendarWithBuilders(),
          const SizedBox(height: 8.0),
          Expanded(child: _buildEventList()),
        ],
      ),
    );
  }

  // Simple TableCalendar configuration (using Styles)
  /*Widget _buildTableCalendar() {
    return TableCalendar(
      locale: 'en_US',
      events: _visibleEvents,
      holidays: _visibleHolidays,
      initialCalendarFormat: CalendarFormat.month,
      formatAnimation: FormatAnimation.slide,
      startingDayOfWeek: StartingDayOfWeek.monday,
      availableGestures: AvailableGestures.all,
      availableCalendarFormats: const {
        CalendarFormat.month: 'Month',
        CalendarFormat.twoWeeks: '2 weeks',
        CalendarFormat.week: 'Week',
      },
      calendarStyle: CalendarStyle(
        holidayStyle: TextStyle(color: Colors.amber),
        selectedColor: Colors.lightBlueAccent[200],
        todayColor: Colors.deepPurple[200],
        markersColor: Colors.redAccent[700],
      ),
      headerStyle: HeaderStyle(
        formatButtonTextStyle: TextStyle().copyWith(color: Colors.white, fontSize: 15.0),
        formatButtonDecoration: BoxDecoration(
          color: Colors.tealAccent[400],
          borderRadius: BorderRadius.circular(16.0),
        ),
      ),
      onDaySelected: _onDaySelected,
      onVisibleDaysChanged: _onVisibleDaysChanged,
    );
  }*/

  // More advanced TableCalendar configuration (using Builders & Styles)
  Widget _buildTableCalendarWithBuilders() {
    return TableCalendar(
      locale: 'en_US',
      events: _visibleEvents,
      holidays: _visibleHolidays,
      initialCalendarFormat: CalendarFormat.month,
      formatAnimation: FormatAnimation.slide,
      startingDayOfWeek: StartingDayOfWeek.monday,
      availableGestures: AvailableGestures.all,
      availableCalendarFormats: const {
        CalendarFormat.month: '',
        CalendarFormat.week: '',
      },
      calendarStyle: CalendarStyle(
        outsideDaysVisible: false,
        weekendStyle: TextStyle().copyWith(color: Colors.blue[800]),
        holidayStyle: TextStyle().copyWith(color: Colors.blue[800]),
      ),
      daysOfWeekStyle: DaysOfWeekStyle(
        weekendStyle: TextStyle().copyWith(color: Colors.blue[600]),
      ),
      headerStyle: HeaderStyle(
        centerHeaderTitle: true,
        formatButtonVisible: false,
      ),
      builders: CalendarBuilders(
        selectedDayBuilder: (context, date, _) {
          return FadeTransition(
            opacity: Tween(begin: 0.0, end: 1.0).animate(_controller),
            child: Container(
              margin: const EdgeInsets.all(4.0),
              padding: const EdgeInsets.only(top: 5.0, left: 6.0),
              color: Colors.amberAccent[300],
              width: 100,
              height: 100,
              child: Text(
                '${date.day}',
                style: TextStyle().copyWith(fontSize: 16.0),
              ),
            ),
          );
        },
        todayDayBuilder: (context, date, _) {
          return Container(
            margin: const EdgeInsets.all(4.0),
            padding: const EdgeInsets.only(top: 5.0, left: 6.0),
            color: Colors.amber[400],
            width: 100,
            height: 100,
            child: Text(
              '${date.day}',
              style: TextStyle().copyWith(fontSize: 16.0),
            ),
          );
        },
        markersBuilder: (context, date, events, holidays) {
          final children = <Widget>[];

          if (events != null) {
            children.add(
              Positioned(
                right: 1,
                bottom: 1,
                child: _buildEventsMarker(date, events),
              ),
            );
          }

          if (holidays != null) {
            children.add(
              Positioned(
                right: -2,
                top: -2,
                child: _buildHolidaysMarker(),
              ),
            );
          }

          return children;
        },
      ),
      onDaySelected: (date, events) {
        _onDaySelected(date, events);
        _controller.forward(from: 0.0);
      },
      onVisibleDaysChanged: _onVisibleDaysChanged,
    );
  }

  Widget _buildEventsMarker(DateTime date, List events) {
    return AnimatedContainer(
      duration: const Duration(milliseconds: 300),
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: Utils.isSameDay(date, _selectedDay)
            ? Colors.brown[500]
            : Utils.isSameDay(date, DateTime.now()) ? Colors.brown[300] : Colors.blue[400],
      ),

    );
  }

  Widget _buildHolidaysMarker() {
    return AnimatedContainer(
      duration: const Duration(milliseconds: 300),
      child: new Container(
        color: Colors.transparent,
        child: new Container(
          decoration: new BoxDecoration(
              border: Border.all(color: Colors.black),
              shape: BoxShape.circle
          ),
        ),),
      /*decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: Utils.isSameDay(date, _selectedDay)
            ? Colors.brown[500]
            : Utils.isSameDay(date, DateTime.now()) ? Colors.brown[300] : Colors.blue[400],
      ),*/

    );
  }

  Widget _buildEventList() {
    return ListView(
      children: _selectedEvents
          .map((event) => Container(
        decoration: BoxDecoration(
          border: Border.all(width: 0.8),
          borderRadius: BorderRadius.circular(12.0),
        ),
        margin: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
        child: ListTile(
          title: Text(event.toString()),
          onTap: () => print('$event tapped!'),
        ),
      ))
          .toList(),
    );
  }
}