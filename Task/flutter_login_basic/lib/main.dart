import 'views/Home.dart';
import 'package:flutter_login_basic/model/person.dart';
import 'package:flutter/material.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  final Future<Person> post;

  MyApp({Key key, this.post}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      debugShowCheckedModeBanner: false,
      routes: <String, WidgetBuilder>{},
      home: Home(sId: 'TMNflro8MSIZOSAMYq2Fg'), //Home(sId: 'kID46Zahh8MvbyNHHOe9Rg',key: this.key,),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => new _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> with WidgetsBindingObserver {
  Person person;

  //ModelPerson modelPerson;
  var mobile = TextEditingController();
  var password = TextEditingController();
  var userType = 'Doctor';

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    super.didChangeAppLifecycleState(state);
    switch (state) {
      case AppLifecycleState.paused:
        print('paused state');
        break;
      case AppLifecycleState.resumed:
        print('resumed state');
        /*Navigator.push(
            context, MaterialPageRoute(builder: (context) => PassCodeScreen()));*/
        break;
      case AppLifecycleState.inactive:
        print('inactive state');
        break;
      case AppLifecycleState.suspending:
        print('suspending state');
        break;
    }
  }

  @override
  void dispose() {
    // Clean up the controller when the Widget is disposed
    WidgetsBinding.instance.removeObserver(this);
    mobile.dispose();
    password.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        body: SingleChildScrollView(
      child: Container(
          child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            child: Container(
              padding: EdgeInsets.fromLTRB(15.0, 110.0, 0.0, 0.0),
              child: Text('Login',
                  style:
                      TextStyle(fontSize: 60.0, fontWeight: FontWeight.bold)),
            ),
          ),
          Container(
              padding: EdgeInsets.only(top: 35.0, left: 20.0, right: 20.0),
              child: Column(
                children: <Widget>[
                  TextField(
                    controller: mobile,
                    decoration: InputDecoration(
                        hintText: "Use the following format example@site.com",
                        labelText: 'Mobile*',
                        labelStyle: TextStyle(
                            fontWeight: FontWeight.bold, color: Colors.grey),
                        focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.green))),
                  ),
                  SizedBox(height: 20.0),
                  TextField(
                    controller: password,
                    decoration: InputDecoration(
                        labelText: 'Password*',
                        labelStyle: TextStyle(
                            fontWeight: FontWeight.bold, color: Colors.grey),
                        focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.green))),
                    obscureText: true,
                  ),
                  SizedBox(height: 5.0),
                  SizedBox(height: 40.0),
                  SizedBox(height: 20.0),
                ],
              )),
          SizedBox(height: 15.0),
          Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(top: 15.0, left: 20.0),
                    child: Text(
                      'Forgot your password',
                    ),
                  ),
                  FlatButton(
                    padding: EdgeInsets.only(right: 20.0),
                    onPressed: () {
                      _buttonPressed();
                    },
                    child: Container(
                      height: 40.0,
                      width: 60.0,
                      child: Material(
                        color: Colors.greenAccent,
                        child: Center(
                          child: Text(
                            'LOGIN',
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(width: 5.0),
              Row(children: <Widget>[
                Container(
                  padding: EdgeInsets.only(top: 10.0, left: 20.0),
                  child: Text(
                    'Did not receive your activation link?',
                  ),
                ),
              ])
            ],
          ),
        ],
      )),
    ));
  }

  void _buttonPressed() async {
    print(mobile.text);
    print(password.text);
    person = await fetchPost('8582852477', 'Usha@1992', userType);
    await Navigator.push(context,
        MaterialPageRoute(builder: (context) => Home(sId: person.securityID)));
  }
}
