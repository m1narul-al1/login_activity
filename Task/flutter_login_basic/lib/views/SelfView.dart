import 'package:flutter/material.dart';
import 'package:flutter_login_basic/model/PersonalDetails.dart';
import 'package:flutter_login_basic/model/FamilyMember.dart';
import 'package:flutter_login_basic/views/SearchView.dart';

class SelfView extends StatefulWidget {
  final sId;

  SelfView({this.sId});

  @override
  _SelfViewState createState() => _SelfViewState(sId: sId);
}

class _SelfViewState extends State<SelfView> {
  final sId;

  PersonalDetails personalDetails;
  var familyMemberResponce;

  _SelfViewState({this.sId});

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if (personalDetails == null) {
      fetchPostPersonalDetails(sId).then((result) {
        print(result);
        setState(() {
          personalDetails = result;
        });
      });
    }
    if (familyMemberResponce == null) {
      fetchPostFamilyMember(sId).then((result) {
        print(result);
        setState(() {
          familyMemberResponce = result;
        });
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('SelfView'),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[_buildSelf(), Divider(), _buildMember()],
        ),
      ),
    );
  }

  _buildSelf() {
    if (personalDetails != null) {
      return ListTile(
        onTap: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => SearchView(personalDetails: sId)));
        },
        leading: Container(
            width: 50.0,
            height: 50.0,
            decoration: new BoxDecoration(
                shape: BoxShape.circle,
                image: personalDetails.personal.profilePicture != null
                    ? new DecorationImage(
                        fit: BoxFit.fill,
                        image: new NetworkImage(
                            personalDetails.personal.profilePicture),
                      )
                    : Container())),
        title: Text(
            '${personalDetails.personal.firstName} ${personalDetails.personal.lastName}'),
        subtitle: Text(personalDetails.personal.officePhone),
      );
    } else {
      return Center(child: CircularProgressIndicator());
    }
  }

  _buildMember() {
    if (familyMemberResponce != null) {
      if (familyMemberResponce is List) {
        return ListView.builder(
          shrinkWrap: true,
          itemCount: familyMemberResponce.length,
          itemBuilder: (BuildContext context, int index) {
            return Container(
                margin:
                    const EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
                child: ListTile(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => SearchView(
                                  personalDetails: sId,
                                  familyMember:
                                      familyMemberResponce[index].userID,
                                )));
                  },
                  contentPadding: EdgeInsets.fromLTRB(4.0, 4.0, 4.0, 0.0),
                  leading: Container(
                      width: 50.0,
                      height: 50.0,
                      decoration: new BoxDecoration(
                          shape: BoxShape.circle,
                          image:
                              familyMemberResponce[index].profilePicture != null
                                  ? new DecorationImage(
                                      fit: BoxFit.fill,
                                      image: new NetworkImage(
                                          familyMemberResponce[index]
                                              .profilePicture),
                                    )
                                  : null)),
                  title: Text(
                    '${familyMemberResponce[index].memberName}',
                    style: TextStyle(fontSize: 18),
                  ),
                  subtitle: Text(
                    'Relation : ${familyMemberResponce[index].relationName}',
                    style: TextStyle(fontSize: 16),
                  ),
                ));
          },
        );
      }else{
        return Text(familyMemberResponce.faildReson);
      }
    } else {
      return Center(child: CircularProgressIndicator());
    }
  }
}
