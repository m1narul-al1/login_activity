import 'package:flutter/material.dart';
import 'package:flutter_login_basic/model/person.dart';
import 'package:flutter_login_basic/model/changePasswordModel.dart';
import 'package:flutter_login_basic/views/Home.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:toast/toast.dart';

class ChangePassword extends StatelessWidget{
   final String sId;
   var currentpassword = TextEditingController();
   var newpassword = TextEditingController();
  ChangePasswordModel changePasswordModel;
   Person person;

  ChangePassword({Key key, @required this.sId}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Change Password"),
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Container(
                padding: EdgeInsets.only(top: 35.0, left: 20.0, right: 20.0),
                child: Column(
                  children: <Widget>[
                    Text('SecurityID :'+sId),
                    TextField(
                      obscureText: true,
                      controller: currentpassword,
                      decoration: InputDecoration(
                          labelText: 'Current Password*',
                          labelStyle: TextStyle(
                              fontWeight: FontWeight.bold, color: Colors.grey),
                          focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.green))),
                    ),
                    SizedBox(height: 20.0),
                    TextField(
                      controller: newpassword,
                      decoration: InputDecoration(
                          labelText: 'New Password*',
                          labelStyle: TextStyle(
                              fontWeight: FontWeight.bold, color: Colors.grey),
                          focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.green))),
                      obscureText: true,
                    ),
                    SizedBox(height: 20.0),
                    RaisedButton(
                      color: Colors.greenAccent,
                      onPressed: () {
                        _changemypassword(context);
                      },
                      child: Text('Submit'),
                    ),
                  ],
                )),
          ],
        ),
      ),
    );
  }

  void _changemypassword(BuildContext context) async{
    changePasswordModel = await fetchPostPWD(sId,currentpassword.text,newpassword.text);
    //person.securityID = changePasswordModel.newSecurityID;
    if(changePasswordModel.status=="Success"){
      Toast.show("Update completed",context,duration: Toast.LENGTH_SHORT,gravity: Toast.CENTER,backgroundColor: Colors.greenAccent,textColor: Colors.white);
    }else{
      Toast.show("Update failed",context,duration: Toast.LENGTH_SHORT,gravity: Toast.CENTER,backgroundColor: Colors.red,textColor: Colors.white);
    }
    await Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => Home(sId: sId,)));
    //print(" SID :"+sId+" Current :"+currentpassword.text+" New Pass :"+newpassword.text);


  }

   Future<ChangePasswordModel> fetchPostPWD(String securityID,String currentPassword,String newpassword) async {
     print(securityID);
     print(currentPassword);
     print(newpassword);
     var uri = Uri.http("cure-staging-api.azurewebsites.net", Uri.encodeFull("/api/account/ChangePassword"), { "SecurityID" : securityID, "CurrentPassword" : currentPassword,"NewPassword" : newpassword });
     print(uri);
     final response =
     await http.post(uri);
     print(response.statusCode);
     print(response.body);
     if (response.statusCode == 200) {
       // If server returns an OK response, parse the JSON
       return ChangePasswordModel.fromJson(json.decode(response.body));
     } else {
       // If that response was not OK, throw an error.
       throw Exception('Failed to load post');
     }

   }
}
