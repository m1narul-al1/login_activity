import 'dart:async';

import 'package:flutter/material.dart';
import 'package:passcode_screen/passcode_screen.dart';

class PassCodeScreen extends StatefulWidget {
  @override
  _PassCodeScreenState createState() => _PassCodeScreenState();
  }
class _PassCodeScreenState extends State<PassCodeScreen>{
  @override
  void dispose() {
    _verificationNotifier.close();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('PassCode'),
      ),
      body: PasscodeScreen(
        title: "PassCode",
        passwordEnteredCallback: _onPasscodeEntered,
        cancelLocalizedText: 'Cancel',
        deleteLocalizedText: 'Delete',
        shouldTriggerVerification: _verificationNotifier.stream,
      ),
      // This trailing comma makes auto-formatting nicer for build methods.
    );
  }

  final StreamController<bool> _verificationNotifier = StreamController<bool>.broadcast();

  _onPasscodeEntered(String enteredPasscode) {
    bool isValid = '123456' == enteredPasscode;
    _verificationNotifier.add(isValid);
  }

}
