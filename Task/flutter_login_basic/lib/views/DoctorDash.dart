import 'package:flutter/material.dart';

class DoctorDash extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    bool isSwitched = true;
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Doctor"),
      ),
      body: SafeArea(
          child: SizedBox.expand(
              child: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Container(
              color: Colors.lightBlueAccent,
              child: Row(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      Text(
                        "Online",
                        style: TextStyle(color: Colors.white70),
                      ),
                    ],
                  ),
                  Column(
                    children: <Widget>[
                      Container(
                          width: 80.0,
                          height: 80.0,
                          margin: EdgeInsets.only(top:20.0,bottom: 10.0),
                          decoration: new BoxDecoration(
                              color: Colors.green,
                              shape: BoxShape.circle,
                              image: new DecorationImage(
                                  fit: BoxFit.fill,
                                  image: NetworkImage(
                                      'http://www.cndajin.com/data/wls/44/7134442.png')))),
                    ],
                  ),
                  Column(
                    children: <Widget>[
                      Switch(
                        value: isSwitched,
                        onChanged: (value) {
                          setState(() {
                            isSwitched = value;
                          });
                        },
                        activeTrackColor: Colors.white70,
                        activeColor: Colors.green,
                      ),
                    ],
                  ),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.only(bottom:12.0),
              color: Colors.lightBlueAccent,
              child: Row(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Text(
                    'Dr. Simon Doe | Dentist',
                    style: TextStyle(color: Colors.white70,fontSize: 18,),
                  ),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.only(top:20.0,bottom: 20.0),
              child: Row(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      Container(

                        height: 130.0,
                        width: 130.0,
                        decoration: BoxDecoration(
                            /*border: Border.all(
                                width: 1.0
                            ),*/
                            color: Colors.green,
                            borderRadius: BorderRadius.circular(10.0),
                        ),
                        child: _buildChild('You Have','6 ', 'clinic','Manage Clinic'),
                      ),
                    ],
                  ),
                  Column(
                    children: <Widget>[
                      Container(

                        height: 130.0,
                        width: 130.0,
                        decoration: BoxDecoration(
                          /*border: Border.all(
                                width: 1.0
                            ),*/
                          color: Colors.greenAccent,
                          borderRadius: BorderRadius.circular(10.0),
                        ),

                        child: _buildChild('December' ,'02 ', '2017','Manage eConsult'),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.only(bottom: 20.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      Container(
                        height: 130.0,
                        width: 130.0,
                        decoration: BoxDecoration(
                          /*border: Border.all(
                                width: 1.0
                            ),*/
                          color: Colors.lightGreen,
                          borderRadius: BorderRadius.circular(10.0),
                        ),

                        child: _buildChild('Your Credit','19.21','K','View Bills'),
                      ),
                    ],
                  ),
                  Column(
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Container(
                        height: 130.0,
                        width: 130.0,
                        decoration: BoxDecoration(
                          /*border: Border.all(
                                width: 1.0
                            ),*/
                          color: Colors.lightGreenAccent,
                          borderRadius: BorderRadius.circular(10.0),
                        ),

                        child: _buildChild('Patient queue','23 ','Today','View Patient'),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            Container(
              height: 50.0,
              padding: EdgeInsets.all(10.0),
              color: Colors.lightBlueAccent,
              child: Row(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Text(
                        'Manage Payment',
                        style: TextStyle(color: Colors.white70),
                      ),
                    ],
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      Text(
                        'Manage Account',
                        style: TextStyle(color: Colors.white70),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ))),
    );
  }

  void setState(Null Function() param0) {}

  Widget _buildChild(String start,String mid,String end, String manage) {
    return Stack(
      alignment: AlignmentDirectional.lerp(AlignmentDirectional.center,AlignmentDirectional.topEnd, 1.4),
      children: <Widget>[
        /*Container(
            width: 40.0,
            height: 40.0,
            margin: EdgeInsets.all(12),
            child: IconButton(
              icon: Icon(Icons.add),
            ),
            decoration: new BoxDecoration(
              shape: BoxShape.rectangle,
              borderRadius: BorderRadius.circular(10.0),
              color: Colors.purpleAccent,
            )),*/
        Container(
          width: 130.0,
          height: 130.0,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Row(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Text('$start',style: TextStyle(color: Colors.black,fontSize: 16),),
                ],
              ),
              Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text('$mid',style: TextStyle(color: Colors.purple,fontSize: 24,textBaseline: TextBaseline.alphabetic),),
                  Text('$end',style: TextStyle(color: Colors.black,fontSize: 16,textBaseline: TextBaseline.alphabetic)),
                ],
              ),
          new Divider(
            color: Colors.purpleAccent,),
              Row(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Text('$manage',style: TextStyle(color: Colors.black),)
                ],
              ),
            ],
          ),
          margin: EdgeInsets.all(10.0),
        )
      ],
    );
  }
}
