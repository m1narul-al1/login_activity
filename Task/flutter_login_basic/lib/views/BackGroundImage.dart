import 'dart:ui' as ui;
import 'package:flutter/material.dart';
import 'package:flutter_login_basic/main.dart';

class BackDetailsPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Image.asset('assets/login.jpeg', fit: BoxFit.cover),
          BackdropFilter(
            filter: ui.ImageFilter.blur(sigmaX: 5.0, sigmaY: 5.0),
            child: Container(
              color: Colors.black.withOpacity(0.5),
              // TODO: child: _buildContent(),
              child: new MyHomePage(),
            ),
          ),
        ],
      ),
    );
  }

}