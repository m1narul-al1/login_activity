import 'package:flutter/material.dart';
import 'package:flutter_login_basic/model/AppointmentBookByPatientModel.dart';
import 'package:flutter_login_basic/model/GetSlots.dart';

class Slots extends StatefulWidget {
  final doctorId, chamberId;
  final personalDetails;
  final familyMember;

  Slots({this.doctorId, this.chamberId,this.familyMember,this.personalDetails});

  _SlotsState createState() =>
      new _SlotsState(doctorId: doctorId, chamberId: chamberId,familyMember: familyMember,personalDetails: personalDetails);
}

class _SlotsState extends State<Slots> {
  final doctorId, chamberId;
  final personalDetails;
  final familyMember;

  _SlotsState({this.doctorId, this.chamberId,this.personalDetails,this.familyMember});

  var media;
  var _slots;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if (_slots == null) {
      fetchSlots(id: doctorId, chamberId: chamberId).then((result) {
        //print(result);
        if (result is CommonDataReturn) {
          setState(() {
            _slots = result;
          });
          print(result.faildReson);
        }
        if (result is List) {
          //print(result);
          setState(() {
            _slots = result;
            print(_slots);
            //print(allSpecializationModel);
          });
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    media = MediaQuery.of(context).size;
    // TODO: implement build
    //print(_slots.length);
    return Scaffold(
        appBar: AppBar(
          title: Text("Slots"),
        ),
        body: _slots == null
            ? Center(child: CircularProgressIndicator())
            : _slots is CommonDataReturn
                ? Center(
                    child: Text(_slots.faildReson),
                  )
                : ListView.builder(
                    itemCount: _slots?.length,
                    itemBuilder: (BuildContext context, int index) {
                      return Container(
                        height: 100,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              margin: EdgeInsets.all(10.0),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                mainAxisSize: MainAxisSize.max,
                                children: <Widget>[
                                  Text('Date: ' + _slots[index]?.workDate,
                                      style: TextStyle(
                                          color: Colors.green, fontSize: 20)),
                                ],
                              ),
                            ),
                            Container(
                              height: 55.0,
                              child: ListView.builder(
                                scrollDirection: Axis.horizontal,
                                shrinkWrap: true,
                                itemCount: _slots[index]?.slots?.length,
                                itemBuilder: (BuildContext context, int idx) {
                                  return Container(
                                    margin: EdgeInsets.all(10.0),
                                    child: RawChip(
                                      showCheckmark: false,
                                      label: Text(
                                        _slots[index]?.slots[idx].time,
                                        style: TextStyle(
                                            color: !_slots[index]
                                                    ?.slots[idx]
                                                    .available
                                                ? Colors.white
                                                : Colors.blue,
                                            fontWeight: FontWeight.bold),
                                      ),
                                      backgroundColor:
                                          !_slots[index]?.slots[idx].available
                                              ? Colors.blue
                                              : Colors.white,
                                      shape: StadiumBorder(
                                          side: BorderSide(
                                              color: Colors.blue, width: 2.0)),
                                      selectedColor: Colors.blue,
                                      selected:
                                          !_slots[index]?.slots[idx].available,
                                      onPressed: () {
                                        //print(_slots[index]?.slots[idx].available);
                                        if(_slots[index]?.slots[idx].available){
                                          _showDialog(
                                              workDate: _slots[index].workDate,
                                              workshiftId:
                                              _slots[index].workshiftId,
                                              time:
                                              _slots[index]?.slots[idx].time);
                                        }else{
                                          _showDialogNotValid(workDate: _slots[index].workDate,
                                              workshiftId:
                                              _slots[index].workshiftId,
                                              time:
                                              _slots[index]?.slots[idx].time);
                                        }

                                      },
                                    ),
                                  );
                                },
                              ),
                            ),
                          ],
                        ),
                      );
                    },
                  ));
  }

  void _showDialog({String workDate, int workshiftId, String time}) {
    //print(familyMember);
    //print(personalDetails);
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: new Text("Alart!"),
          content: new Text("Do you want to book on $workDate in $time"),
          actions: <Widget>[
            new FlatButton(
              child: new Text("Cancel"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            new FlatButton(
              child: new Text("Yes"),
              onPressed: () async {
                await bookSlot(
                  memberID: familyMember,
                  securityID: personalDetails,
                        time: time,
                        workDate: workDate,
                        workshiftId: workshiftId)
                    .then((result) {
                  print(result);
                });
                fetchSlots(id: doctorId, chamberId: chamberId).then((result) {
                  //print(result);
                  if (result is CommonDataReturn) {
                    setState(() {
                      _slots = result;
                    });
                    print(result.faildReson);
                  }
                  if (result is List) {
                    //print(result);
                    setState(() {
                      _slots = result;
                      print(_slots);
                      //print(allSpecializationModel);
                    });
                  }
                });
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  void _showDialogNotValid({String workDate, int workshiftId, String time}) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: new Text("Error!"),
          content: new Text("$workDate in $time is Already Booked."),
          actions: <Widget>[
            new FlatButton(
              child: new Text("Got it"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}
