import 'package:flutter/material.dart';
import 'package:flutter_login_basic/model/GetPendingListModel.dart';


class PendingAppointments extends StatefulWidget {
  String sId;

  PendingAppointments(this.sId);

  PendingAppointmentsState createState() => new PendingAppointmentsState(sId);
}

class PendingAppointmentsState extends State<PendingAppointments> {
  String sId;
  List<PendingList> pendingList;

  @override
  Widget build(BuildContext context) {
    var futureBuilder = FutureBuilder(
      future: fetchGetPendingList(sId),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.none:
          case ConnectionState.waiting:
            return new Center(
                child: CircularProgressIndicator(
              backgroundColor: Colors.cyanAccent,
            ));
          default:
            if (snapshot.hasError)
              return new Text('Error: ${snapshot.error}');
            else {
              if (snapshot.data is List) {
                return createPendingAppiontmentsListView(context, snapshot);
              } else
                return createChamberResponse(context, snapshot);
            }
        }
      },
    );
    // TODO: implement build
    return Scaffold(
        appBar: AppBar(
          title: const Text('Pending Appointments'),
          actions: <Widget>[
            IconButton(
              onPressed: () {},
              icon: Icon(
                Icons.add,
                color: Colors.white,
              ),
            ),
          ],
        ),
        body: futureBuilder);
  }

  PendingAppointmentsState(this.sId);
}



Widget createPendingAppiontmentsListView(BuildContext context, AsyncSnapshot snapshot) {
  List<PendingList> values = snapshot.data;
  return ListView.builder(
    itemCount: values.length,
    itemBuilder: (BuildContext context, int index) {
      return new Column(
        children: <Widget>[
          new ListTile(
            /*leading: CircleAvatar(
        radius: 30.0,
        backgroundImage:
        NetworkImage(values[index].profilePicture),
        backgroundColor: Colors.transparent,
      ),*/
            title: Text(values[index].firstName +" "+ values[index].lastName),
            subtitle: Text(values[index].contactNo),
          ),
          Divider(
            height: 2.0,
          ),
        ],
      );
    },
  );
}

Widget createChamberResponse(BuildContext context, AsyncSnapshot snapshot) {
  return Row(
    mainAxisAlignment: MainAxisAlignment.center,
    crossAxisAlignment: CrossAxisAlignment.center,
    mainAxisSize: MainAxisSize.max,
    children: <Widget>[
      Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Text('No Pending Appointments'),
        ],
      )
    ],
  );
}
