import 'package:flutter/material.dart';
import 'package:flutter_login_basic/model/DoctorSearchModel.dart';
import 'package:flutter_login_basic/views/FilterView.dart';
import 'package:flutter_login_basic/views/Slots.dart';

class DoctorView extends StatefulWidget {
  final personalDetails;
  final familyMember;

  DoctorView({this.personalDetails, this.familyMember});

  @override
  _DoctorViewState createState() => _DoctorViewState(personalDetails: personalDetails,familyMember: familyMember);
}

class _DoctorViewState extends State<DoctorView> {
  final personalDetails;
  final familyMember;

  _DoctorViewState({this.personalDetails, this.familyMember});

  MediaQueryData queryData;
  Map returnedValue;
  int countFilter;
  var _doctorList;
  String _location = 'Durgapur';
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if (_doctorList == null) {
      fetchDoctor(returnedValue: returnedValue, location: _location)
          .then((result) {
        print(result);
        setState(() {
          if (result is CommonDataReturn) {
            _doctorList = result;
            print(result.faildReson);
          }
          if (result is List) {
            _doctorList = result;
          }
        });
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    queryData = MediaQuery.of(context);
    return Scaffold(
      appBar: AppBar(
        title: Text('Search: Doctor'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.location_on),
            onPressed: () {},
          ),
          Center(
              child: Text(
            _location,
            style: TextStyle(fontSize: 18),
          ))
        ],
      ),
      body: Column(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Container(
              height: queryData.size.height * .08,
              color: Colors.cyan[400],
              child: Center(
                child: Text(
                  '${_doctorList != null ? _doctorList.length : 0} results found in $_location',
                  style: TextStyle(fontSize: 24),
                ),
              )),
          /*Container(
              margin: EdgeInsets.all(10.0),
              padding: EdgeInsets.all(5.0),
              child: Center(
                child: Text(
                  'Location : $_location',
                  style: TextStyle(fontSize: 20),
                ),
              )),*/
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Container(
                      margin: EdgeInsets.only(left: 10.0),
                      child: Text(
                        '${countFilter != null ? countFilter : 0} filter selected',
                        style: TextStyle(fontSize: 16),
                      )),
                  IconButton(
                    icon: Icon(
                      Icons.delete_forever,
                      size: 16,
                    ),
                    onPressed: () {
                      setState(() {
                        returnedValue = null;
                        countFilter = 0;
                        print(returnedValue);
                        fetchDoctor(
                                returnedValue: returnedValue,
                                location: _location)
                            .then((result) {
                          print(result);
                          setState(() {
                            if (result is CommonDataReturn) {
                              print(result.faildReson);
                            }
                            if (result is List) {
                              _doctorList = result;
                            }
                          });
                        });
                      });
                    },
                  ),
                ],
              ),
              IconButton(
                icon: Icon(
                  Icons.filter_list,
                  color: Colors.deepPurple,
                  size: 24.0,
                ),
                onPressed: _filter,
              )
            ],
          ),
          _doctorList == null
              ? Center(
                  child: Text('No doctor available'),
                )
              : _doctorList is CommonDataReturn
                  ? Center(
                      child: Text(_doctorList.faildReson),
                    )
                  : _buildDoctorList(context),
        ],
      ),
    );
  }

  _filter() async {
    returnedValue = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => FilterView(returnedValue: returnedValue)));
    setState(() {
      returnedValue = returnedValue;
    });
    fetchDoctor(returnedValue: returnedValue, location: _location)
        .then((result) {
      print(result);
      setState(() {
        if (result is CommonDataReturn) {
          print(result.faildReson);
          _doctorList = result;
        }
        if (result is List) {
          _doctorList = result;
        }
      });
    });
    if (returnedValue != null) {
      countFilter = (returnedValue['spvalue'] != null ? 1 : 0) +
              (returnedValue['minValue'] != null ? 1 : 0) +
              (returnedValue['maxValue'] != null ? 1 : 0) +
              (returnedValue['customText'].isNotEmpty
                  ? 1
                  : 0) /*+
          (returnedValue['specialitymodel'] != null ? 1 : 0)*/
          ;
    }
    print(returnedValue);
  }

  Widget _buildDoctorList(BuildContext context) {
    return Expanded(
      child: ListView.builder(
        shrinkWrap: true,
        itemCount: _doctorList.length,
        itemBuilder: (BuildContext context, int index) {
          return Container(
              height: queryData.size.height * 0.25,
              decoration: BoxDecoration(
                color:
                    (index % 2 == 0) ? Colors.green : Colors.deepPurpleAccent,
                border: Border.all(width: 0.8),
                borderRadius: BorderRadius.circular(12.0),
              ),
              margin:
                  const EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
              child: Column(
                children: <Widget>[
                  ListTile(
                    contentPadding: EdgeInsets.fromLTRB(4.0, 4.0, 4.0, 0.0),
                    leading: Container(
                        width: 50.0,
                        height: 50.0,
                        decoration: new BoxDecoration(
                            shape: BoxShape.circle,
                            image: _doctorList[index].picture != null
                                ? new DecorationImage(
                                    fit: BoxFit.fill,
                                    image: new NetworkImage(
                                        _doctorList[index].picture),
                                  )
                                : null)),
                    title: Text(
                      '${_doctorList[index].fullName}',
                      style: TextStyle(fontSize: 18),
                    ),
                    subtitle: Text(
                      'Spacilization : ${_doctorList[index].specialization}',
                      style: TextStyle(fontSize: 16),
                    ),
                  ),
                  Text(
                      '${_doctorList[index].experience} years of exprience, Fees: ${_doctorList[index].fees}'),
                  Text('Available at : ${_doctorList[index].chamberName}'),
                  Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      RaisedButton(
                        child: Text(
                          'View Profile',
                          style: TextStyle(color: Colors.white),
                        ),
                        onPressed: null,
                        shape: RoundedRectangleBorder(
                            borderRadius:
                                BorderRadiusDirectional.circular(10.0)),
                      ),
                      RaisedButton(
                        child: Text(
                          'Book Appointment',
                          style: TextStyle(color: Colors.white),
                        ),
                        onPressed: () => _bookDoctor(_doctorList[index]),
                        shape: RoundedRectangleBorder(
                            borderRadius:
                                BorderRadiusDirectional.circular(10.0)),
                      ),
                    ],
                  ),
                ],
              ));
        },
      ),
    );
  }

  _bookDoctor(doctorList) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => Slots(
              personalDetails: personalDetails,
                  familyMember: familyMember,
                  doctorId: doctorList.id,
                  chamberId: doctorList.chamberId,
                )));
  }
}
