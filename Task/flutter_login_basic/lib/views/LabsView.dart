import 'package:flutter/material.dart';
import 'package:flutter_login_basic/model/LabSearchModel.dart';
import 'package:flutter_login_basic/views/LabsFilterView.dart';

class LabsView extends StatefulWidget {
  @override
  _LabsViewState createState() => _LabsViewState();
}

class _LabsViewState extends State<LabsView> {
  MediaQueryData queryData;
  var labsSearch;
  var _location = 'Durgapur';
  var returnedValue;

  int countFilter;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    print(labsSearch);
    if(labsSearch == null){
      fetchLabsSearch(returnedValue: returnedValue, location: _location)
          .then((result) {
        print(result);
        setState(() {
          if (result is CommonDataReturn) {
            print(result.faildReson);
          }
          if (result is List) {
            labsSearch = result;
          }
        });
      });
    }
  }
  @override
  Widget build(BuildContext context) {
    queryData = MediaQuery.of(context);
    return Scaffold(
      appBar: AppBar(
        title: Text('Search: Labs'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.location_on),
            onPressed: () {},
          ),
          Center(
              child: Text(
                _location,
                style: TextStyle(fontSize: 18),
              ))
        ],
      ),
      body:Column(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Container(
              height: queryData.size.height * .08,
              color: Colors.cyan[400],
              child: Center(
                child: Text(
                  '${labsSearch != null ? labsSearch.length : 0} results found in $_location',
                  style: TextStyle(fontSize: 24),
                ),
              )),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Container(
                      margin: EdgeInsets.only(left: 10.0),
                      child: Text(
                        '${countFilter != null ? countFilter : 0} filter selected',
                        style: TextStyle(fontSize: 16),
                      )),
                  IconButton(
                    icon: Icon(
                      Icons.delete_forever,
                      size: 16,
                    ),
                    onPressed: () {
                      setState(() {
                        returnedValue = null;
                        countFilter = 0;
                        print(returnedValue);
                        fetchLabsSearch(
                            returnedValue: returnedValue,
                            location: _location)
                            .then((result) {
                          print(result);
                          setState(() {
                            if (result is CommonDataReturn) {
                              print(result.faildReson);
                            }
                            if (result is List) {
                              labsSearch = result;
                            }
                          });
                        });
                      });
                    },
                  ),
                ],
              ),
              IconButton(
                icon: Icon(
                  Icons.filter_list,
                  color: Colors.deepPurple,
                  size: 24.0,
                ),
                onPressed: _filter,
              )
            ],
          ),
          labsSearch == null
              ? Center(
            child: Text('No labs available'),
          )
              : _buildLabsList(context),
        ],
      ),
    );
  }
  Widget _buildLabsList(BuildContext context) {
    return Expanded(
      child: ListView.builder(
        shrinkWrap: true,
        itemCount: labsSearch?.length,
        itemBuilder: (BuildContext context, int index) {
          return Container(
              height: queryData.size.height * 0.25,
              decoration: BoxDecoration(
                color:
                (index % 2 == 0) ? Colors.green : Colors.deepPurpleAccent,
                border: Border.all(width: 0.8),
                borderRadius: BorderRadius.circular(12.0),
              ),
              margin:
              const EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
              child: ListTile(
                title: Text(labsSearch[index].storeName),
                subtitle: Text(labsSearch[index].id),
              ));
        },
      ),
    );
  }

  _filter() async {
    returnedValue = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => LabsFilterView(returnedValue: returnedValue)));
    setState(() {
      returnedValue = returnedValue;
    });
    fetchLabsSearch(returnedValue: returnedValue, location: _location)
        .then((result) {
      print(result);
      setState(() {
        if (result is CommonDataReturn) {
          print(result.faildReson);
        }
        if (result is List) {
          labsSearch = result;
        }
      });
    });
    if (returnedValue != null) {
      countFilter = (returnedValue['spvalue'] != null ? 1 : 0) +
          (returnedValue['minValue'] != null ? 1 : 0) +
          (returnedValue['maxValue'] != null ? 1 : 0) +
          (returnedValue['customText'].isNotEmpty
              ? 1
              : 0) /*+
          (returnedValue['specialitymodel'] != null ? 1 : 0)*/
      ;
    }
    print(returnedValue);
  }
}
