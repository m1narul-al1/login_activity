import 'package:flutter/material.dart';
import 'package:flutter_login_basic/model/ChamberModel.dart';

class ChamberView extends StatefulWidget {
  final String sId;

  ChamberView({this.sId});

  ChamberViewState createState() => new ChamberViewState(sId: sId);
}

class ChamberViewState extends State<ChamberView> {
  List<ChamberModel> chamberList;
  MediaQueryData queryData;
  final String sId;

  ChamberViewState({this.sId});

  @override
  Widget build(BuildContext context) {
    queryData = MediaQuery.of(context);
    var futureBuilder = FutureBuilder(
      future: fetchPostChamber(sId),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.none:
          case ConnectionState.waiting:
            return new Center(
                child: CircularProgressIndicator(
              backgroundColor: Colors.cyanAccent,
            ));
          default:
            if (snapshot.hasError)
              return new Text('Error: ${snapshot.error}');
            else {
              if (snapshot.data is List) {
                return createChamberListView(context, snapshot);
              } else
                return createChamberResponse(context, snapshot);
            }
        }
      },
    );
    // TODO: implement build
    return Scaffold(
        appBar: AppBar(
          title: const Text('Chamber'),
          actions: <Widget>[
            IconButton(
              onPressed: () {},
              icon: Icon(
                Icons.add,
                color: Colors.white,
              ),
            ),
          ],
        ),
        body: futureBuilder);
  }
}

Widget createChamberListView(BuildContext context, AsyncSnapshot snapshot) {
  List<ChamberModel> values = snapshot.data;
  return ListView.builder(
    itemCount: values.length,
    itemBuilder: (BuildContext context, int index) {
      return new Column(
        children: <Widget>[
          new ListTile(
            title: Text(values[index].chamberName),
            subtitle: Text(values[index].phoneNumber),
          ),
          Divider(
            height: 2.0,
          ),
        ],
      );
    },
  );
}

Widget createChamberResponse(BuildContext context, AsyncSnapshot snapshot) {
  return Row(
    mainAxisAlignment: MainAxisAlignment.center,
    crossAxisAlignment: CrossAxisAlignment.center,
    mainAxisSize: MainAxisSize.max,
    children: <Widget>[
      Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Text('No Chamber Added'),
          RaisedButton(
            onPressed: () {},
            color: Colors.greenAccent,
            child: Text('Add Chamber'),
          )
        ],
      )
    ],
  );
}
