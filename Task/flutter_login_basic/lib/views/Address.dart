import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_login_basic/model/CityResponse.dart';
import 'package:flutter_login_basic/model/CountryResponce.dart';
import 'package:flutter_login_basic/model/PersonalDetails.dart';
import 'package:flutter_login_basic/model/StateResponse.dart';
import 'package:toast/toast.dart';
import 'package:passcode_screen/passcode_screen.dart';
import 'package:autocomplete_textfield/autocomplete_textfield.dart';

class Address extends StatefulWidget {
  final String sId;
  final Key key;

  Address({this.key, @required this.sId}) : super(key: key);

  _AddressState createState() => new _AddressState(sId);
}

class _AddressState extends State<Address> with WidgetsBindingObserver {
  StateListModel _selectedValueState;
  List<CityList> _cityList;
  CountryListModel _selectedValueCountry;
  List<StateListModel> _addressState;
  List<CountryListModel> _addressCountry;
  int countryIndex;
  var indexOfCountry, indexOfState, indexOfCity;
  PersonalDetails personalDetails;
  AppLifecycleState _passCodeMethod;
  var sId;
  //bool showOther = false;
  TextEditingController controller = new TextEditingController();
  GlobalKey<AutoCompleteTextFieldState<CityList>> key = new GlobalKey();

  _AddressState(this.sId);

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    setState(() {
      _passCodeMethod = state;
    });
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    _setAll();
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    _verificationNotifier.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    AutoCompleteTextField searchTextField;
    // TODO: implement build
    //print(_passCodeMethod);
    if (_passCodeMethod != null) {
      return PasscodeScreen(
        title: 'Enter Your Passcode',
        passwordDigits: 4,
        passwordEnteredCallback: _onPasscodeEntered,
        cancelLocalizedText: 'Cancel',
        deleteLocalizedText: 'Delete',
        shouldTriggerVerification: _verificationNotifier.stream,
      );
    }
    return Scaffold(
        appBar: AppBar(
          title: Text("Address"),
        ),
        body: Container(
          child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              _cityList == null
                  ? CircularProgressIndicator()
                  : searchTextField = AutoCompleteTextField<CityList>(
                      clearOnSubmit: false,
                      style: new TextStyle(color: Colors.black, fontSize: 16.0),
                      decoration: new InputDecoration(
                          suffixIcon: Container(
                            width: 85.0,
                            height: 60.0,
                          ),
                          contentPadding:
                              EdgeInsets.fromLTRB(10.0, 30.0, 10.0, 20.0),
                          filled: true,
                          hintText: 'City Name',
                          hintStyle: TextStyle(color: Colors.black)),
                      itemBuilder: (context, item) {
                        return Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(
                              item.cityName,
                              style: TextStyle(fontSize: 16.0),
                            ),
                            Padding(
                              padding: EdgeInsets.all(15.0),
                            ),
                            Text(
                              item.stateName,
                            )
                          ],
                        );
                      },
                      itemFilter: (item, query) {
                        /*if(searchTextField.textField.controller.text.length ==3){
                          if (item.cityName
                              .toLowerCase()
                              .startsWith(query.toLowerCase()) ==
                              true) {
                            setState(() {
                              showOther = true;
                            });
                          }
                        }*/
                        return item.cityName
                            .toLowerCase()
                            .startsWith(query.toLowerCase());
                      },
                      itemSorter: (a, b) {
                        return a.cityName.compareTo(b.cityName);
                      },
                      itemSubmitted: (item) {
                        setState(() {
                          searchTextField.textField.controller.text =
                              item.cityName;
                          indexOfCity = _addressState
                              .indexWhere((state) => state.id == item.stateID);
                          print(indexOfCity);
                          _selectedValueState = _addressState[indexOfCity];
                        });
                      },
                      suggestions: _cityList.toList(),
                      key: key,
                      submitOnSuggestionTap: true,
                    ),
              /*showOther == true
                  ? TextField(
                      autofocus: true,
                decoration: new InputDecoration(
                    suffixIcon: Container(
                      width: 85.0,
                      height: 60.0,
                    ),
                    contentPadding:
                    EdgeInsets.fromLTRB(10.0, 30.0, 10.0, 20.0),
                    filled: true,
                    hintText: 'Other city name',
                    hintStyle: TextStyle(color: Colors.black)),
              )
                  : Text(''),*/
              ListTile(
                title: Text('State'),
                trailing: DropdownButton<StateListModel>(
                  value: _selectedValueState,
                  hint: Text('Select State'),
                  onChanged: ((StateListModel newValue) {
                    setState(() {
                      _selectedValueState = newValue;
                      print(_selectedValueState);
                      print(_selectedValueState.id);
                      print(_selectedValueState.stateName);
                      print(_selectedValueState.countryID);
                      print(_selectedValueState.countryName);
                      print(_addressState
                          .contains(_selectedValueState)); // => true
                      indexOfCountry = _addressCountry.indexWhere(
                          (state) => state.id == _selectedValueState.countryID);
                      print(indexOfCountry);
                      _selectedValueCountry = _addressCountry[indexOfCountry];
                    });
                  }),
                  items: this._addressState?.map((StateListModel value) {
                    return new DropdownMenuItem<StateListModel>(
                      value: value,
                      child: Text(value.stateName),
                    );
                  })?.toList(),
                ),
              ),
              ListTile(
                title: Text('Country'),
                trailing: DropdownButton<CountryListModel>(
                  value:
                      _selectedValueCountry
                  ,
                  hint: Text('Select Country'),
                  onChanged: ((CountryListModel newValue) {
                    setState(() {
                      _selectedValueCountry = newValue;
                      print(_selectedValueCountry);
                    });
                  }),
                  items: this._addressCountry?.map((CountryListModel value) {
                    return new DropdownMenuItem<CountryListModel>(
                      value: value,
                      child: new Text(value.countryName),
                    );
                  })?.toList(),
                ),
              ),
            ],
          ),
        ));
  }

  final StreamController<bool> _verificationNotifier =
      StreamController<bool>.broadcast();

  _onPasscodeEntered(String enteredPasscode) {
    bool isValid = '1234' == enteredPasscode;
    _verificationNotifier.add(isValid);
  }

  void _setAll() async {
    //bool _state = false,_country = false,_personal = false;
    if (_addressState == null) {
      List<StateListModel> _addressState = await fetchPostState();
      print(_addressState);
      //_state = true;
      setState(() {
        this._addressState = _addressState;
      });
    }
    if (_addressCountry == null) {
      List<CountryListModel> _addressCountry = await fetchPostCountry();
      print(_addressCountry);
      //_country = true;
      setState(() {
        this._addressCountry = _addressCountry;
      });
    }
    if (personalDetails == null) {
      personalDetails = await fetchPostPersonalDetails(sId);
      //_personal = true;
    }
    if (_cityList == null) {
      List<CityList> _cityList = await fetchPostCity();
      print(_cityList);
      setState(() {
        this._cityList = _cityList;
      });
      //_personal = true;
    }

    //print(_personal == true && _country == true && _state == true);

    if (personalDetails != null &&
        _addressState != null &&
        _addressCountry != null) {
      setState(() {
        //print(personalDetails.Status);
        if (personalDetails.status == 'Success') {
          indexOfState = _addressState.indexWhere(
              (state) => state.stateName == personalDetails.personal.state);
          print(indexOfState);
          _selectedValueState = _addressState[indexOfState];
          indexOfCountry = _addressCountry
              .indexWhere((state) => state.id == _selectedValueState.countryID);
          print(indexOfCountry);
          _selectedValueCountry = _addressCountry[indexOfCountry];
        } else {
          Toast.show(personalDetails.faildReson, context);
        }
      });
    }
  }
}
