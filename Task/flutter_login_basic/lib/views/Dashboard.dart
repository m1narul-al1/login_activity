import 'package:flutter/material.dart';
import 'package:flutter_login_basic/model/PersonalDetails.dart';

class Dashboard extends StatelessWidget {
  PersonalDetails personalDetails;
  var familyMemberResponce;

  Dashboard(
      {Key key, @required this.personalDetails, this.familyMemberResponce})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        appBar: AppBar(
          title: Text("Dashboard"),
        ),
        body: SafeArea(
            child: SizedBox.expand(
          child: Row(
            children: <Widget>[
              Container(
                  width: 80.0,
                  height: 80.0,
                  margin: EdgeInsets.all(10.0),
                  decoration: new BoxDecoration(
                      color: Colors.green,
                      shape: BoxShape.circle,
                      image: new DecorationImage(
                          fit: BoxFit.fill,
                          image: NetworkImage(
                              personalDetails.personal.profilePicture)))),
              _buildChild() ?? Text('No family member'),
            ],
          ),
        )));
  }

  Widget _buildChild() {
    if (familyMemberResponce is List) {
      if (familyMemberResponce.length > 1) {
        return Row(
          children: <Widget>[
            _buildFamilyMember(familyMemberResponce[0].profilePicture),
            _buildFamilyMember(familyMemberResponce[1].profilePicture),
          ],
        );
      } else if (familyMemberResponce.length == 1) {
        return Row(
          children: <Widget>[
            _buildFamilyMember(familyMemberResponce[0].profilePicture),
            _buildAddMember(),
          ],
        );
      }
    }
    return _buildAddMember();
  }

  Widget _buildFamilyMember(String url) {
    return Row(
      children: <Widget>[
        Container(
            width: 80.0,
            height: 80.0,
            margin: EdgeInsets.all(10.0),
            decoration: new BoxDecoration(
                shape: BoxShape.circle,
                color: Colors.green,
                image: new DecorationImage(
                    fit: BoxFit.fill, image: new NetworkImage(url)))),
      ],
    );
  }

  Widget _buildAddMember() {
    return Container(
        width: 80.0,
        height: 80.0,
        margin: EdgeInsets.all(10.0),
        child: IconButton(
          onPressed: () {},
          icon: Icon(Icons.add),
        ),
        decoration: new BoxDecoration(
          shape: BoxShape.circle,
          color: Colors.greenAccent,
        ));
  }
}
