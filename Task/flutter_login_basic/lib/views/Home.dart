import 'package:flutter/material.dart';
import 'package:flutter_login_basic/model/FamilyMember.dart';
import 'package:flutter_login_basic/model/PersonalDetails.dart';
import 'package:flutter_login_basic/model/appointmentController.dart';
import 'package:flutter_login_basic/views/CalenderView.dart';
import 'package:flutter_login_basic/views/Camera.dart';
import 'package:flutter_login_basic/views/ChamberView.dart';
import 'package:flutter_login_basic/views/Dashboard.dart';
import 'package:flutter_login_basic/views/DoctorDash.dart';
import 'package:flutter_login_basic/views/ChangePassword.dart';
import 'package:flutter_login_basic/views/EditProfile.dart';
import 'package:flutter_login_basic/views/Address.dart';
import 'package:flutter_login_basic/views/MultipleSelect.dart';
import 'package:flutter_login_basic/views/PendingAppointments.dart';
import 'package:flutter_login_basic/views/Search.dart';
import 'package:flutter_login_basic/views/SelfView.dart';
import 'package:flutter_login_basic/views/Slots.dart';
import 'package:flutter_login_basic/views/Weekday.dart';

class Home extends StatelessWidget {
  //final Person modelPerson;
  AppointmentMap appointmentMap;
  final String sId;
  PersonalDetails personalDetails;
  var familyMemberResponce;
  Home({Key key, @required this.sId}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Home"),
        ),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Center(
                    child: Container(
                  child: Text('SecurityID :' + sId),
                ))
              ],
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                RaisedButton(
                  color: Colors.greenAccent,
                  onPressed: () {
                    _buttonPressed();
                  },
                  child: Text('Show appointments'),
                ),
                FlatButton(
                  color: Colors.greenAccent,
                  onPressed: () {
                    _changepass(context);
                  },
                  child: Text('Change password'),
                ),
              ],
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                RaisedButton(
                  color: Colors.greenAccent,
                  onPressed: () {
                    _editProfile(context);
                  },
                  child: Text('Edit Profile'),
                ),
                RaisedButton(
                  color: Colors.greenAccent,
                  onPressed: () {
                    _dashboard(context);
                  },
                  child: Text('Dashboard'),
                ),
                RaisedButton(
                  color: Colors.greenAccent,
                  onPressed: () {
                    _doctorDashboard(context);
                  },
                  child: Text('Doctor Dashboard'),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                RaisedButton(
                  color: Colors.greenAccent,
                  onPressed: () {
                    _addressPressed(context);
                  },
                  child: Text('Address'),
                ),
                RaisedButton(
                  color: Colors.greenAccent,
                  onPressed: () {
                    _chamberPressed(context);
                  },
                  child: Text('Chamber'),
                ),
                RaisedButton(
                  color: Colors.greenAccent,
                  onPressed: () {
                    _pendingAppointmentPressed(context);
                  },
                  child: Text('Pending Appoinment'),
                ),

              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                RaisedButton(
                  color: Colors.greenAccent,
                  onPressed: () {
                    _calenderPressed(context);
                  },
                  child: Text('Calender View'),
                ),
                RaisedButton(
                  color: Colors.greenAccent,
                  onPressed: () {
                    _weekdayPressed(context);
                  },
                  child: Text('Weekday function'),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                RaisedButton(
                  color: Colors.greenAccent,
                  onPressed: () {
                    _slotsPressed(context);
                  },
                  child: Text('Get Slots'),
                ),
                RaisedButton(
                  color: Colors.greenAccent,
                  onPressed: () {
                    _cameraAppPressed(context);
                  },
                  child: Text('CameraApp'),
                ),
                RaisedButton(
                  color: Colors.greenAccent,
                  onPressed: () {
                    _searchPressed(context);
                  },
                  child: Text('Directory'),
                ),

              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                RaisedButton(
                  color: Colors.greenAccent,
                  onPressed: () {
                    _searchViewPressed(context);
                  },
                  child: Text('Search'),
                ),
                RaisedButton(
                  color: Colors.greenAccent,
                  onPressed: () {
                    multipleSelect(context);
                  },
                  child: Text('Multiple Select'),
                ),
                RaisedButton(
                  color: Colors.greenAccent,
                  onPressed: () {
                    cCAPayment(context);
                  },
                  child: Text('Payment'),
                ),
              ],
            ),

          ],
        ));
  }

  void _buttonPressed() async {
    appointmentMap = await fetchPostAppointment(sId);
    print(appointmentMap);
  }

  void _changepass(BuildContext context) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ChangePassword(
                  sId: sId,
                )));
  }

  void _editProfile(BuildContext context) async {
    personalDetails = await fetchPostPersonalDetails(sId);
    print(personalDetails);
    await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) =>
                EditProfile(sId: sId, personalDetails: personalDetails)));
  }

  void _dashboard(BuildContext context) async {
    //print(familyMemberResponce);
    _handleSignIn().whenComplete(() => Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => Dashboard(
                personalDetails: personalDetails,
                familyMemberResponce: familyMemberResponce))));
  }

  _handleSignIn() async {
    personalDetails = await fetchPostPersonalDetails(sId);
    print(personalDetails);
    familyMemberResponce = await fetchPostFamilyMember(sId);
  }

  void _doctorDashboard(BuildContext context) {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => DoctorDash()));
  }

  void _addressPressed(BuildContext context) {
    Navigator.push(context, MaterialPageRoute(builder: (context) => Address(sId: sId,)));
  }

  void _chamberPressed(BuildContext context) {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => ChamberView(sId: sId)));
  }
  void _pendingAppointmentPressed(BuildContext context) {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => PendingAppointments(sId)));
  }
  void _calenderPressed(BuildContext context) {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => CalendarViewApp(securityID: sId,)));
  }

  void _weekdayPressed(BuildContext context) {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => Weekdays()));
  }

  void _slotsPressed(BuildContext context) {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => Slots()));
  }

  void _cameraAppPressed(BuildContext context) async {

    await Navigator.push(
        context, MaterialPageRoute(builder: (context) => PickImageDemo()));
  }

  void _searchPressed(BuildContext context) {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => Search(sId)));
  }

  void _searchViewPressed(BuildContext context) {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => SelfView(sId: sId,)));
  }

  void multipleSelect(BuildContext context) {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => MultipleSelect()));
  }

  void cCAPayment(BuildContext context) {

  }


}


