import 'package:flutter/material.dart';
import 'package:flutter_login_basic/model/PharmaSearchModel.dart';
import 'package:flutter_login_basic/views/PharmacyFilterView.dart';
class PharmacyView extends StatefulWidget {
  @override
  _PharmacyViewState createState() => _PharmacyViewState();
}

class _PharmacyViewState extends State<PharmacyView> {
  MediaQueryData queryData;
  var pharmaSearch;
  var _location = 'Durgapur';
  var returnedValue;

  int countFilter;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    print(pharmaSearch);
    if(pharmaSearch == null){
      fetchPharmaSearch(returnedValue: returnedValue, location: _location)
          .then((result) {
        print(result);
        setState(() {
          if (result is CommonDataReturn) {
            print(result.faildReson);
          }
          if (result is List) {
            pharmaSearch = result;
          }
        });
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    queryData = MediaQuery.of(context);
    return Scaffold(
      appBar: AppBar(
        title: Text('Search: Pharmacy'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.location_on),
            onPressed: () {},
          ),
          Center(
              child: Text(
                _location,
                style: TextStyle(fontSize: 18),
              ))
        ],
      ),
      body:Column(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Container(
              height: queryData.size.height * .08,
              color: Colors.cyan[400],
              child: Center(
                child: Text(
                  '${pharmaSearch != null ? pharmaSearch.length : 0} results found in $_location',
                  style: TextStyle(fontSize: 24),
                ),
              )),
          /*Container(
              margin: EdgeInsets.all(10.0),
              padding: EdgeInsets.all(5.0),
              child: Center(
                child: Text(
                  'Location : $_location',
                  style: TextStyle(fontSize: 20),
                ),
              )),*/
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Container(
                      margin: EdgeInsets.only(left: 10.0),
                      child: Text(
                        '${countFilter != null ? countFilter : 0} filter selected',
                        style: TextStyle(fontSize: 16),
                      )),
                  IconButton(
                    icon: Icon(
                      Icons.delete_forever,
                      size: 16,
                    ),
                    onPressed: () {
                      setState(() {
                        returnedValue = null;
                        countFilter = 0;
                        print(returnedValue);
                        fetchPharmaSearch(
                            returnedValue: returnedValue,
                            location: _location)
                            .then((result) {
                          print(result);
                          setState(() {
                            if (result is CommonDataReturn) {
                              print(result.faildReson);
                            }
                            if (result is List) {
                              pharmaSearch = result;
                            }
                          });
                        });
                      });
                    },
                  ),
                ],
              ),
              IconButton(
                icon: Icon(
                  Icons.filter_list,
                  color: Colors.deepPurple,
                  size: 24.0,
                ),
                onPressed: _filter,
              )
            ],
          ),
          pharmaSearch == null
              ? Center(
            child: Text('No pharmacy available'),
          )
              : _buildPharmaList(context),
        ],
      ),
    );
  }
  Widget _buildPharmaList(BuildContext context) {
    return Expanded(
      child: ListView.builder(
        shrinkWrap: true,
        itemCount: pharmaSearch?.length,
        itemBuilder: (BuildContext context, int index) {
          return InkWell(
            onTap: ()=>print('${pharmaSearch[index]['ID']}'),
            child: Container(
                height: queryData.size.height * 0.25,
                decoration: BoxDecoration(
                  color:
                  (index % 2 == 0) ? Colors.green : Colors.deepPurpleAccent,
                  border: Border.all(width: 0.8),
                  borderRadius: BorderRadius.circular(12.0),
                ),
                margin:
                const EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
                child: ListTile(
                  title: Text(pharmaSearch[index]['StoreName']),
                  subtitle: Text(pharmaSearch[index]['City']),
                ),
            ),
          );
        },
      ),
    );
  }


  _filter() async {
    returnedValue = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => PharmacyFilterView(returnedValue: returnedValue)));
    setState(() {
      returnedValue = returnedValue;
    });
    fetchPharmaSearch(returnedValue: returnedValue, location: _location)
        .then((result) {
      print(result);
      setState(() {
        if (result is CommonDataReturn) {
          print(result.faildReson);
        }
        if (result is List) {
          pharmaSearch = result;
        }
      });
    });
    if (returnedValue != null) {
      countFilter = (returnedValue['spvalue'] != null ? 1 : 0) +
          (returnedValue['minValue'] != null ? 1 : 0) +
          (returnedValue['maxValue'] != null ? 1 : 0) +
          (returnedValue['customText'].isNotEmpty
              ? 1
              : 0) /*+
          (returnedValue['specialitymodel'] != null ? 1 : 0)*/
      ;
    }
    print(returnedValue);
  }
}
