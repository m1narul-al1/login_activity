import 'package:flutter/material.dart';

class MultipleSelect extends StatefulWidget {
  @override
  _MultipleSelectState createState() => _MultipleSelectState();
}

class _MultipleSelectState extends State<MultipleSelect> {
  List multiValue = [
    '24/7 doctor available',
    'Doctors on call',
    'Instant Availability'
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('MultipleSelect'),
        centerTitle: true,
      ),
      body: _buildFeatures(multiValue),
    );
  }

  _buildFeatures(List values) {
    bool _selectedIdxA;
    bool _selectedIdxB;
    bool _selectedIdxC;
    int indexA = values.indexWhere((test) => test == '24/7 doctor available');
    //print(indexA);
    _selectedIdxA = indexA != -1 ? true : false;
    int indexB = values.indexWhere((test) => test == 'Doctors on call');
    //print(indexB);
    _selectedIdxB = indexB != -1 ? true : false;
    int indexC = values.indexWhere((test) => test == 'Instant Availability');
    //print(indexC);
    _selectedIdxC = indexC != -1 ? true : false;
    return Wrap(
      spacing: 5.0,
      direction: Axis.horizontal,
      crossAxisAlignment: WrapCrossAlignment.start,
      children: <Widget>[
        RawChip(
          showCheckmark: false,
          label: Text(
            '24/7 doctor available',
            style: TextStyle(
                color: _selectedIdxA ? Colors.white : Colors.blue,
                fontWeight: FontWeight.bold),
          ),
          backgroundColor: _selectedIdxA ? Colors.blue : Colors.white,
          shape:
              StadiumBorder(side: BorderSide(color: Colors.blue, width: 2.0)),
          selectedColor: Colors.blue,
          selected: _selectedIdxA,
          onPressed: () {
            setState(() {
              _selectedIdxA != true
                  ? values.add('24/7 doctor available')
                  : values.remove('24/7 doctor available');
              _selectedIdxA = !_selectedIdxA;
              multiValue = values;
              print(multiValue);
            });
          },
        ),
        RawChip(
          showCheckmark: false,
          label: Text(
            'Doctors on call',
            style: TextStyle(
                color: _selectedIdxB ? Colors.white : Colors.blue,
                fontWeight: FontWeight.bold),
          ),
          backgroundColor: _selectedIdxB ? Colors.blue : Colors.white,
          shape:
              StadiumBorder(side: BorderSide(color: Colors.blue, width: 2.0)),
          selectedColor: Colors.blue,
          selected: _selectedIdxB,
          onPressed: () {
            setState(() {
              _selectedIdxB != true
                  ? values.add('Doctors on call')
                  : values.remove('Doctors on call');
              _selectedIdxB = !_selectedIdxB;
              multiValue = values;

              print(multiValue);
            });
          },
        ),
        RawChip(
          showCheckmark: false,
          label: Text(
            'Instant Availability',
            style: TextStyle(
                color: _selectedIdxC ? Colors.white : Colors.blue,
                fontWeight: FontWeight.bold),
          ),
          backgroundColor: _selectedIdxC ? Colors.blue : Colors.white,
          shape:
              StadiumBorder(side: BorderSide(color: Colors.blue, width: 2.0)),
          selectedColor: Colors.blue,
          selected: _selectedIdxC,
          onPressed: () {
            setState(() {
              _selectedIdxC != true
                  ? values.add('Instant Availability')
                  : values.remove('Instant Availability');
              _selectedIdxC = !_selectedIdxC;
              multiValue = values;
              print(multiValue);
            });
          },
        ),
        RaisedButton(
          child: Text('Click'),
          onPressed: (){print('Clicked:$multiValue');},
        ),
      ],
    );
  }
}
