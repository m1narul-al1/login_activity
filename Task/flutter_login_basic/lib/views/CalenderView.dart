import 'package:flutter/material.dart';
import 'package:flutter_login_basic/model/GetClinicList.dart';
import 'package:flutter_login_basic/model/GetDistinctDays.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:table_calendar/table_calendar.dart';

void main() {
  initializeDateFormatting().then((_) => runApp(CalendarViewApp()));
}

class CalendarViewApp extends StatefulWidget {
  final String securityID;


  CalendarViewApp({this.securityID});

  @override
  _CalendarViewAppState createState() => _CalendarViewAppState(securityID: securityID);
}

class _CalendarViewAppState extends State<CalendarViewApp> with TickerProviderStateMixin {
  var selectedDay;
  List<String> _bookedDays;
  List<int> _bookedDaysInt = List();
  CommonDataReturn _noEvents;
  DateTime _selectedDay;
  var getDistinctDaysResponse;
  var getClinicList;
  List<ClinicListWithTiming> _events;
  final String securityID;
  _CalendarViewAppState({this.securityID});

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _selectedDay = DateTime.now();
    if (getDistinctDaysResponse == null) {
      getDistinctDaysResponse =
          fetchPostDays(securityID: securityID).then((result) {
        print(result);
        setState(() {
          _bookedDays = result;
          _bookedDays?.forEach((v) => _bookedDaysInInt(v));
        });
      });
    }
    if (getClinicList == null) {
      getClinicList = fetchClinicList(
              currentTime: _selectedDay.toString(), securityID: securityID)
          .then((result) {
        print(result);
        if (result is CommonDataReturn) {
          setState(() {
            _noEvents = result;
          });
          //print(result.faildReson);
        }
        if (result is List<ClinicListWithTiming>) {
          setState(() {
            _events = result;
          });
          //print(result.clinicId);
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Calendar'),
        ),
        body: Container(
          color: Color.fromRGBO(58, 66, 86, 1.0),
          child: Column(
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              _buildTableCalendar(_bookedDaysInt),
              const SizedBox(height: 8.0),
              Expanded(
                  child: _events != null
                      ? _buildEventList()
                      : _noEvents != null
                          ? _buildNoEvent()
                          : Center(
                              child: Text('Loading...'),
                            )),
            ],
          ),
        ));
  }

  Widget _buildTableCalendar(List<int> bookedDays) {
    return TableCalendar(
      daysOfWeekStyle: DaysOfWeekStyle(
          weekendStyle: TextStyle(color: Colors.red),
          weekdayStyle: TextStyle()),
      locale: 'en_US',
      builders: CalendarBuilders(
        todayDayBuilder: (context, date, _) {
          return Container(
            margin: const EdgeInsets.all(4.0),
            padding: const EdgeInsets.only(top: 5.0, left: 6.0),
            color: Colors.transparent,
            child: _buildCurrentDay(date),
          );
        },
        selectedDayBuilder: (context, date, _) {
          return Container(
            margin: const EdgeInsets.all(4.0),
            padding: const EdgeInsets.only(top: 5.0, left: 6.0),
            color: Colors.transparent,
            child: _buildSelectedDay(date),
          );
        },
        dayBuilder: (context, date, _) {
          return Container(
            margin: const EdgeInsets.all(4.0),
            padding: const EdgeInsets.only(top: 5.0, left: 6.0),
            color: Colors.transparent,
            child: isWorkingDay(date, bookedDays)
                ? _notifyDate(date)
                : Center(
                    child: Text(
                    '${date.day}',
                    style: TextStyle(fontSize: 16.0, color: Colors.white),
                  )),
          );
        },
      ),
      initialCalendarFormat: CalendarFormat.month,
      formatAnimation: FormatAnimation.slide,
      startingDayOfWeek: StartingDayOfWeek.monday,
      availableGestures: AvailableGestures.all,
      headerStyle: HeaderStyle(
          centerHeaderTitle: true,
          formatButtonVisible: false,
          titleTextStyle: TextStyle(color: Colors.greenAccent)),
      calendarStyle: CalendarStyle(
        weekdayStyle: TextStyle(color: Colors.lightBlueAccent),
        outsideDaysVisible: false,
        holidayStyle: TextStyle(color: Colors.amber),
        selectedColor: Colors.lightBlueAccent[200],
        todayColor: Colors.deepPurple[200],
      ),
      selectedDay: selectedDay,
      onDaySelected: (date, events) {
        _onDaySelected(date, events);
      },
    );
  }

  isWorkingDay(DateTime date, List<int> bookedDays) {
    switch (bookedDays.length) {
      case 1:
        if (date.weekday == bookedDays[0]) {
          return true;
        } else {
          return false;
        }
        break;
      case 2:
        if (date.weekday == bookedDays[0] || date.weekday == bookedDays[1]) {
          return true;
        } else {
          return false;
        }
        break;
      case 3:
        if (date.weekday == bookedDays[0] ||
            date.weekday == bookedDays[1] ||
            date.weekday == bookedDays[2]) {
          return true;
        } else {
          return false;
        }
        break;
      case 4:
        if (date.weekday == bookedDays[0] ||
            date.weekday == bookedDays[1] ||
            date.weekday == bookedDays[2] ||
            date.weekday == bookedDays[3]) {
          return true;
        } else {
          return false;
        }
        break;
      case 5:
        if (date.weekday == bookedDays[0] ||
            date.weekday == bookedDays[1] ||
            date.weekday == bookedDays[2] ||
            date.weekday == bookedDays[3] ||
            date.weekday == bookedDays[4]) {
          return true;
        } else {
          return false;
        }
        break;
      case 6:
        if (date.weekday == bookedDays[0] ||
            date.weekday == bookedDays[1] ||
            date.weekday == bookedDays[2] ||
            date.weekday == bookedDays[3] ||
            date.weekday == bookedDays[4] ||
            date.weekday == bookedDays[5]) {
          return true;
        } else {
          return false;
        }
        break;
      case 7:
        if (date.weekday == bookedDays[0] ||
            date.weekday == bookedDays[1] ||
            date.weekday == bookedDays[2] ||
            date.weekday == bookedDays[3] ||
            date.weekday == bookedDays[4] ||
            date.weekday == bookedDays[5] ||
            date.weekday == bookedDays[6]) {
          return true;
        } else {
          return false;
        }
        break;
      default:
        return false;
    }
  }

  _notifyDate(DateTime date) {
    return Center(
        child: Container(
      decoration:
          BoxDecoration(shape: BoxShape.circle, color: Colors.lightBlueAccent),
      child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Center(
              child: Text(
                '${date.day}',
                style: TextStyle().copyWith(fontSize: 16.0),
              ),
            ),
          ]),
    ));
  }

  void _onDaySelected(DateTime day, List events) {
    setState(() {
      _selectedDay = day;
      getClinicList = fetchClinicList(
              currentTime: _selectedDay.toString(), securityID: securityID)
          .then((result) {
        print(result);
        if (result is CommonDataReturn) {
          setState(() {
            _noEvents = result;
            _events = null;
          });
          //print(result.faildReson);
        }
        if (result is List<ClinicListWithTiming>) {
          setState(() {
            _events = result;
            _noEvents = null;
          });
          //print(result.clinicId);
        }
      });

      //_selectedEvents = events;
      print(_selectedDay);
      print(_bookedDaysInt);
    });
  }

  _bookedDaysInInt(String v) {
    switch (v.toLowerCase()) {
      case 'mon':
        _bookedDaysInt.add(1);
        break;
      case 'tue':
        _bookedDaysInt.add(2);
        break;
      case 'wed':
        _bookedDaysInt.add(3);
        break;
      case 'thu':
        _bookedDaysInt.add(4);
        break;
      case 'fri':
        _bookedDaysInt.add(5);
        break;
      case 'sat':
        _bookedDaysInt.add(6);
        break;
      case 'sun':
        _bookedDaysInt.add(7);
        break;
    }
  }

  Widget _buildSelectedDay(DateTime date) {
    return Center(
        child: Container(
      decoration:
          BoxDecoration(shape: BoxShape.circle, color: Colors.deepPurple),
      child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Center(
              child: Text(
                '${date.day}',
                style: TextStyle().copyWith(fontSize: 16.0),
              ),
            ),
          ]),
    ));
  }

  Widget _buildCurrentDay(DateTime date) {
    return Center(
        child: Container(
      decoration:
          BoxDecoration(shape: BoxShape.circle, color: Colors.greenAccent),
      child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Center(
              child: Text(
                '${date.day}',
                style: TextStyle().copyWith(fontSize: 16.0),
              ),
            ),
          ]),
    ));
  }

  _buildNoEvent() {
    return Center(
      child: Text('No Event found'),
    );
  }

  Widget _buildEventList() {
    return ListView.builder(
      itemCount: _events.length,
      itemBuilder: (BuildContext context, int index) {
        return Container(
          decoration: BoxDecoration(
            color: (index % 2 == 0)
                ? Color.fromRGBO(64, 75, 96, .9)
                : Colors.deepPurpleAccent,
            border: Border.all(width: 0.8),
            borderRadius: BorderRadius.circular(12.0),
          ),
          margin: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
          child: ListTile(
            title: Text(
              _events[index].clinicName,
              style: TextStyle(color: Colors.white),
            ),
            subtitle: Text(
              _events[index].clinicId.toString(),
              style: TextStyle(color: Colors.white),
            ),
            trailing: Icon(
              Icons.arrow_forward_ios,
              color: Colors.white,
              size: 24.0,
            ),
            onTap: () => print('${_events[index].clinicName} tapped!'),
          ),
        );
      },
    );
  }
}
