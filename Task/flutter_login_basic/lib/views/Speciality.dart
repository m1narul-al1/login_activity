import 'package:flutter/material.dart';
import 'package:flutter_login_basic/model/AllSpecializationModel.dart';
import 'package:grouped_buttons/grouped_buttons.dart';

class Speciality extends StatefulWidget {
  final String checkedValues;

  Speciality({this.checkedValues});

  @override
  _SpecialityState createState() =>
      _SpecialityState(checkedValues: checkedValues);
}

class _SpecialityState extends State<Speciality> {
  MediaQueryData queryData;
  List<String> _specialityValues = List();

  List<AllSpecializationModel> allSpecializationModel;

  _SpecialityState({this.checkedValues});

  String checkedValues;
  bool flag;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    flag = true;
    fetchSpecialization(securityID: 'jf9KeTQULfYVOxgIByuNgA').then((result) {
      //print(result);
      if (result is CommonDataReturn) {
        //print(result.faildReson);
        setState(() {
          flag = false;
        });
      }
      if (result is List<AllSpecializationModel>) {
        //print(result);
        setState(() {
          allSpecializationModel = result;
          allSpecializationModel
              .forEach((element) => _specialityValues.add(element.speciality));
          print(_specialityValues);
          flag = false;
          //print(allSpecializationModel);
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    queryData = MediaQuery.of(context);
    return Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          title: Text('Speciality'),
          centerTitle: true,
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              size: 24,
            ),
            color: Colors.white,
            onPressed: () {
              Navigator.pop(context, checkedValues);
            },
          ),
        ),
        body: ListView(children: <Widget>[
          Container(
            height: (queryData.size.height) * .08,
            padding: const EdgeInsets.only(left: 14.0, top: 14.0),
            child: Text(
              "Basic Speciality",
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0),
            ),
          ),
          Container(
            height: (queryData.size.height) * .8,
            child: flag
                ? Center(
                    child: Container(
                        width: (queryData.size.width) * 1,
                        height: (queryData.size.height) * .1,
                        child: _buildLoading()),
                  )
                : SingleChildScrollView(
                    child: RadioButtonGroup(
                      picked: checkedValues,
                      labels: _specialityValues,
                      onChange: (String label, int index) => print("label: $label index: $index"),
                      onSelected: (String checked) => setState(() {
                            checkedValues = checked;
                          }),
                      itemBuilder: (Radio rb, Text txt, int i) {
                        //print(rb);
                        return Row(
                          children: <Widget>[
                            rb,
                            txt,
                          ],
                        );
                      },
                      //print("checked: ${checked.toString()}"),
                    ),
                  ),
          ),
        ]));
  }

  _buildLoading() {
    return Column(
      children: <Widget>[
        CircularProgressIndicator(),
        Text(
          'Loading...',
          style: TextStyle(fontSize: 18),
          softWrap: true,
        ),
      ],
    );
  }
}
