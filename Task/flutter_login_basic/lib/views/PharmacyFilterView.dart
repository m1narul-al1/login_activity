import 'package:flutter/material.dart';


class PharmacyFilterView extends StatefulWidget {
  final returnedValue;

  PharmacyFilterView({this.returnedValue});

  @override
  _PharmacyFilterViewState createState() =>
      _PharmacyFilterViewState(returnedValue: returnedValue);
}

class _PharmacyFilterViewState extends State<PharmacyFilterView> {
  MediaQueryData queryData;
  var returnedValue;
  int indexOfSp;


  _PharmacyFilterViewState({this.returnedValue});

  final TextEditingController _controller = new TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    if (returnedValue != null) {
      print(returnedValue);
      setState(() {
        returnedValue['customText'].isNotEmpty
            ? _controller.text = returnedValue['customText']
            : _controller.text = null;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    queryData = MediaQuery.of(context);
    return Scaffold(
        appBar: AppBar(
          title: Text('Search : PharmacyFilter'),
          centerTitle: true,
          actions: <Widget>[
            Container(
              alignment: Alignment.centerRight,
              child: IconButton(
                icon: Icon(
                  Icons.add_circle,
                  size: 24,
                ),
                color: Colors.white,
                onPressed: () {
                  returnedValue = {
                    'customText': _controller.text,
                  };
                  Navigator.pop(context, returnedValue);
                },
              ),
            ),
          ],
        ),
        body: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                height: queryData.size.height * 0.35,
                child: Center(
                  child: TextField(
                    textAlign: TextAlign.center,
                    controller: _controller,
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(
                      hintText: 'Custom Search',
                      hintStyle: TextStyle(fontSize: 16),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8),
                          borderSide: BorderSide(
                            width: 1,
                            style: BorderStyle.solid,
                          ),
                          gapPadding: 2.0),
                      filled: true,
                      contentPadding: EdgeInsets.all(16),
                    ),
                  ),
                ),
                margin: EdgeInsets.all(4.0),
                padding: EdgeInsets.all(2.0),
              ),
            ],
          ),
        ));
  }
}
