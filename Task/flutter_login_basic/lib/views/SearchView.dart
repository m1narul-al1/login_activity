import 'package:flutter/material.dart';
import 'package:flutter_login_basic/views/DoctorView.dart';
import 'package:flutter_login_basic/views/LabsView.dart';
import 'package:flutter_login_basic/views/PharmacyView.dart';

class SearchView extends StatefulWidget {
  final personalDetails;
  final familyMember;

  SearchView({this.personalDetails, this.familyMember});

  @override
  _SearchViewState createState() => _SearchViewState(
      familyMember: familyMember, personalDetails: personalDetails);
}

class _SearchViewState extends State<SearchView> {
  MediaQueryData queryData;

  final TextEditingController _controller = new TextEditingController();
  bool _isSearching;
  final personalDetails;
  final familyMember;
  String _searchText;

  _SearchViewState({this.personalDetails, this.familyMember}) {
    _controller.addListener(() {
      if (_controller.text.isEmpty) {
        setState(() {
          _isSearching = false;
          _searchText = null;
        });
      } else {
        setState(() {
          _isSearching = true;
          _searchText = _controller.text;
        });
      }
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _controller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    queryData = MediaQuery.of(context);
    //print(queryData);
    return Scaffold(
        appBar: AppBar(
          title: Text('SearchView'),
          centerTitle: true,
        ),
        body: SingleChildScrollView(
          child: new Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Container(
                child: TextField(
                  textAlign: TextAlign.center,
                  controller: _controller,
                  keyboardType: TextInputType.text,
                  decoration: InputDecoration(
                    hintText: 'Search',
                    hintStyle: TextStyle(fontSize: 16),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8),
                        borderSide: BorderSide(
                          width: 1,
                          style: BorderStyle.solid,
                        ),
                        gapPadding: 2.0),
                    filled: true,
                    contentPadding: EdgeInsets.all(16),
                    suffixIcon: Icon(Icons.search),
                  ),
                  onTap: _handleSearchStart,
                ),
                margin: EdgeInsets.all(4.0),
                padding: EdgeInsets.all(2.0),
              ),
              new Divider(
                color: Colors.red,
              ),
              ListTile(
                title: Center(child: Text('Doctors')),
                trailing: Icon(
                  Icons.arrow_forward_ios,
                  color: Colors.deepPurple,
                  size: 24.0,
                ),
                onTap: () => _doctorPressed(),
              ),
              new Divider(
                color: Colors.red,
              ),
              ListTile(
                title: Center(child: Text('Pharmacy')),
                trailing: Icon(
                  Icons.arrow_forward_ios,
                  color: Colors.deepPurple,
                  size: 24.0,
                ),
                onTap: () => _pharmacyPressed(),
              ),
              new Divider(
                color: Colors.red,
              ),
              ListTile(
                title: Center(child: Text('Labs')),
                trailing: Icon(
                  Icons.arrow_forward_ios,
                  color: Colors.deepPurple,
                  size: 24.0,
                ),
                onTap: () => _labsPressed(),
              ),
              new Divider(
                color: Colors.red,
              ),
            ],
          ),
        ));
  }

  void _handleSearchStart() {
    setState(() {
      _isSearching = true;
    });
  }

  _doctorPressed() {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => DoctorView(
                  familyMember: familyMember,
                  personalDetails: personalDetails,
                )));
  }

  _labsPressed() {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => LabsView()));
  }

  _pharmacyPressed() {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => PharmacyView()));
  }
}
