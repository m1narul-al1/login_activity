import 'package:flutter/material.dart';
import 'package:flutter_login_basic/model/EditProfileStatus.dart';
import 'package:flutter_login_basic/model/PersonalDetails.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:toast/toast.dart';


class EditProfile extends StatelessWidget{
  String sId;
  PersonalDetails personalDetails;
  var name = TextEditingController();
  var gender = TextEditingController();
  var dob = TextEditingController();
  EditProfileStatus editProfileStatus;
  

  EditProfile({Key key, @required this.sId,this.personalDetails}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Edit Profile"),
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Image.network(personalDetails.personal.profilePicture),
            Container(
                padding: EdgeInsets.only(top: 35.0, left: 20.0, right: 20.0),
                child: Column(
                  children: <Widget>[
                    SizedBox(height: 20.0),
                    Text('SecurityID :'+sId),
                    TextField(
                      controller: name,
                      decoration: InputDecoration(
                          labelText: personalDetails.personal.firstName,
                          labelStyle: TextStyle(
                              fontWeight: FontWeight.bold, color: Colors.grey),
                          focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.green)
                          )
                      ),
                    ),
                    SizedBox(height: 20.0),
                    TextField(
                      controller: gender,
                      decoration: InputDecoration(
                          labelText: personalDetails.personal.gender,
                          labelStyle: TextStyle(
                              fontWeight: FontWeight.bold, color: Colors.grey),
                          focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.green))),

                    ),
                    SizedBox(height: 20.0),
/*
                    TextField(
                      controller: dob,
                      decoration: InputDecoration(
                          labelText: personalDetails.personal.DOB,
                          labelStyle: TextStyle(
                              fontWeight: FontWeight.bold, color: Colors.grey),
                          focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.green))),

                    ),
*/
                    SizedBox(height: 20.0),
                    RaisedButton(
                      color: Colors.greenAccent,
                      onPressed: () {
                        _changemyprofile(context);
                      },
                      child: Text('Submit'),
                    ),
                  ],
                )),
          ],
        ),
      ),
    );
  }
  void _changemyprofile(BuildContext context) async {
    editProfileStatus = await fetchPostEditProfileStatus(sId, name.text, gender.text, '', '', '', '', '');
    print(editProfileStatus);
    if(editProfileStatus.status == "Success"){
      Toast.show("Update completed",context,duration: Toast.LENGTH_SHORT,gravity: Toast.CENTER,backgroundColor: Colors.greenAccent,textColor: Colors.white);
    }else{
      Toast.show("Update failed",context,duration: Toast.LENGTH_SHORT,gravity: Toast.CENTER,backgroundColor: Colors.red,textColor: Colors.white);
    }
  }

}

Future<EditProfileStatus> fetchPostEditProfileStatus(String securityID,String name,String genderID,String dOB,String phone,String email,String fileString,String contentType) async {

  var body = json.encode({ "SecurityID" : securityID,"Name" : name,"GenderID": genderID, "DOB" : dOB, "Phone" : phone, "Email" : email,"fileString" : fileString,"contentType" : contentType});
  var uri = Uri.http("cure-staging-api.azurewebsites.net", Uri.encodeFull("/api/Account/EditRootMember"));
  print(uri);
  final response =
  await http.post(uri,headers: {"Content-Type": "application/json"},
      body: body);
  //print(response.body);
  if (response.statusCode == 200) {
    // If server returns an OK response, parse the JSON
    return EditProfileStatus.fromJson(json.decode(response.body));
  } else {
    // If that response was not OK, throw an error.
    throw Exception('Failed to load post');
  }

}
