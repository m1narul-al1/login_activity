import 'package:flutter/material.dart';

class Weekdays extends StatefulWidget {
  final String day;

  Weekdays({this.day});

  _WeekdayState createState() => new _WeekdayState(day);
}

class _WeekdayState extends State<Weekdays> {
  String _selectedValue;
  String _day;
  List<String> _days = [
    'Sunday',
    'Monday',
    'Tuesday',
    'Wednesday',
    'Thursday',
    'Friday',
    'Saturday'
  ];

  _WeekdayState(this._day);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        appBar: AppBar(
          title: Text("Week Day"),
        ),
        body: Container(
          color: Colors.teal,
          child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              ListTile(
                title: Text('Day'),
                trailing: DropdownButton<String>(
                  style: TextStyle(color: Colors.white),
                  value: _day ?? _selectedValue,
                  hint: Text('Select Day'),
                  onChanged: ((String newValue) {
                    setState(() {
                      _selectedValue = newValue;
                    });
                  }),
                  items: this._days?.map((String value) {
                    return new DropdownMenuItem<String>(
                      value: value,
                      child: Text(
                        value,style: TextStyle(color: Colors.black),
                      ),
                    );
                  })?.toList(),
                ),
              ),
            ],
          ),
        ));
  }
}
