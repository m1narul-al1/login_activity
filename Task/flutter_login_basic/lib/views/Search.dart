import 'package:flutter/material.dart';
import 'package:flutter_login_basic/model/DirectorySearchModel.dart';

class Search extends StatefulWidget {
  final sId;

  Search(this.sId);

  SearchState createState() => SearchState(sId);
}

class SearchState extends State<Search> {
final sId;
MediaQueryData queryData;


  Widget appBarTitle = new Text(
    "Search",
    style: new TextStyle(color: Colors.white),
  );
  Icon icon = new Icon(
    Icons.search,
    color: Colors.white,
  );
  final globalKey = new GlobalKey<ScaffoldState>();
  final TextEditingController _controller = new TextEditingController();
  List<DirectorySearchModel> _directoryList;
  bool _isSearching;
  String _searchText;
  List<DirectorySearchModel> searchResult = new List();

  SearchState(this.sId) {
    _controller.addListener(() {
      if (_controller.text.isEmpty) {
        setState(() {
          _isSearching = false;
          _searchText = null;
        });
      } else {
        setState(() {
          _isSearching = true;
          _searchText = _controller.text;
        });
      }
    });
  }

  @override
  void initState() {
    super.initState();
    _isSearching = false;
    getAllPatient(securityID: sId).then((result) {
      //print(result);
      if (result is CommonDataReturn) {
        print(result.faildReson);
      }
      if (result is List<DirectorySearchModel>) {
        //print(result);
        setState(() {
          _directoryList = result;
        });
      }
    });
  }
  @override
  Widget build(BuildContext context) {
    queryData = MediaQuery.of(context);
    return new Scaffold(
        key: globalKey,
        appBar: AppBar(title: Text('Patient Directory'),centerTitle: true,),
        body: new Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Container(
              child: TextField(
                textAlign: TextAlign.center,
                controller: _controller,
                keyboardType: TextInputType.text,
                decoration: InputDecoration(
                  hintText: 'Search',
                  hintStyle: TextStyle(fontSize: 16),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(8),
                    borderSide: BorderSide(
                      width: 1,
                      style: BorderStyle.solid,
                    ),
                    gapPadding: 2.0
                  ),
                  filled: true,
                  contentPadding: EdgeInsets.all(16),
                  suffixIcon: Icon(Icons.search),
                ),
                onTap: _handleSearchStart,
                onChanged: searchOperation,
              ),
              margin: EdgeInsets.all(4.0),
              padding: EdgeInsets.all(2.0),
              height: queryData.size.height * 0.1,
            ),
            Flexible(
                child: searchResult.length != 0 || _controller.text.isNotEmpty
                    ? new ListView.builder(
                        itemCount: searchResult.length,
                        itemBuilder: (BuildContext context, int index) {
                          DirectorySearchModel listData = searchResult[index];
                          return new ListTile(
                            leading: CircleAvatar(
                              radius: 30.0,
                              backgroundImage:
                                  listData?.profilePicture != null
                                      ? NetworkImage(listData?.profilePicture)
                                      : NetworkImage(
                                          'https://via.placeholder.com/150'),
                              backgroundColor: Colors.blue,
                            ),
                            title: listData != null
                                ? Text(listData.name)
                                : Text('Demo Name'),
                            subtitle: listData != null
                                ? Text(listData?.phone)
                                : Text('Demo Text'),
                            onTap: () => print('${listData.name} tapped!'),
                          );
                        },
                      )
                    : _directoryList == null?Center(child: CircularProgressIndicator(),):new ListView.builder(
                        itemCount: _directoryList?.length,
                        itemBuilder: (BuildContext context, int index) {
                          DirectorySearchModel listData =
                              _directoryList != null
                                  ? _directoryList[index]
                                  : null;
                          return new ListTile(
                            leading: CircleAvatar(
                              radius: 30.0,
                              backgroundImage:
                                  listData?.profilePicture != null
                                      ? NetworkImage(listData?.profilePicture)
                                      : NetworkImage(
                                          'https://via.placeholder.com/150'),
                              backgroundColor: Colors.blue,
                            ),
                            title: listData != null
                                ? Text(listData.name)
                                : Text('Demo Name'),
                            subtitle: listData != null
                                ? Text(listData?.phone)
                                : Text('Demo Text'),
                            onTap: () => print('${listData.name} tapped!'),
                          );
                        },
                      ))
          ],
        ));
  }
  void _handleSearchStart() {
    setState(() {
      _isSearching = true;
    });
  }
  void searchOperation(String searchText) {
    //print(searchText);
    searchResult.clear();
    if (_isSearching != null) {
      for (int i = 0; i < _directoryList.length; i++) {
        String data = _directoryList[i].name;
        if (data.toLowerCase().contains(searchText.toLowerCase())) {
          searchResult.add(_directoryList[i]);
        }
      }
    }
  }
}
