import 'package:flutter/material.dart';
import 'package:flutter_login_basic/model/AllSpecializationModel.dart';
import 'package:flutter_login_basic/views/RangeSliderItem.dart';
import 'package:flutter_login_basic/views/Speciality.dart';

class FilterView extends StatefulWidget {
  final returnedValue;

  FilterView({this.returnedValue});

  @override
  _FilterViewState createState() =>
      _FilterViewState(returnedValue: returnedValue);
}

class _FilterViewState extends State<FilterView> {
  MediaQueryData queryData;
  List<String> _speciality;
  String _sp;
  int minValue = 1;
  int maxValue =1000;
  var returnedValue;
  //List<AllSpecializationModel> allSpecializationModel;
  AllSpecializationModel selectedSpecialization;
  int indexOfSp;


  _FilterViewState({this.returnedValue});

  final TextEditingController _controller = new TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    /*fetchSpecialization(securityID: 'jf9KeTQULfYVOxgIByuNgA').then((result) {
      //print(result);
      if (result is CommonDataReturn) {
        //print(result.faildReson);
        setState(() {});
      }
      if (result is List<AllSpecializationModel>) {
        //print(result);
        setState(() {
          allSpecializationModel = result;
          //print(allSpecializationModel);
          if (selectedSpecialization != null) {
            indexOfSp = allSpecializationModel.indexWhere(
                    (_selectedSpecialization) => _selectedSpecialization.speciality == selectedSpecialization.speciality);
            //print(indexOfSp);
            selectedSpecialization = allSpecializationModel[indexOfSp];
          }
        });
      }
    });*/
    if (returnedValue != null) {
      print(returnedValue);
      setState(() {
        returnedValue['minValue'] != null
            ? minValue = returnedValue['minValue']
            : minValue = null;
        returnedValue['maxValue'] != null
            ? maxValue = returnedValue['maxValue']
            : maxValue = null;
        returnedValue['customText'].isNotEmpty
            ? _controller.text = returnedValue['customText']
            : _controller.text = null;
        returnedValue['spvalue'] != null
            ? _sp = returnedValue['spvalue']
            : _sp = null;
        //print(selectedSpecialization.speciality);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    print(selectedSpecialization?.speciality);
    queryData = MediaQuery.of(context);
    return Scaffold(
        appBar: AppBar(
          title: Text('Search : FilterView'),
          centerTitle: true,
          actions: <Widget>[
            Container(
              alignment: Alignment.centerRight,
              child: IconButton(
                icon: Icon(
                  Icons.add_circle,
                  size: 24,
                ),
                color: Colors.white,
                onPressed: () {
                  returnedValue = {
                    //'speciality': _speciality,
                    'minValue': minValue,
                    'maxValue': maxValue,
                    'customText': _controller.text,
                    'specialitymodel': selectedSpecialization,
                    'spvalue':_sp,
                  };
                  Navigator.pop(context, returnedValue);
                },
              ),
            ),
          ],
        ),
        body: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                  margin: EdgeInsets.all(5.0),
                  height: queryData.size.height * 0.05,
                  child: Text(
                    'Speciality :',
                    style: TextStyle(fontSize: 22),
                  )),
              Container(
                height: queryData.size.height * 0.12,
                child: Center(
                  child: _sp == null
                      ? Container(
                          margin: EdgeInsets.only(left: 8.0),
                          child: Text(
                            'No speciality Selected',
                            style: TextStyle(fontSize: 16),
                          ))
                      : Chip(
                    padding: EdgeInsets.all(10.0),
                    label: Text(_sp),

                  ),
                )
                /*ListView.builder(
                        scrollDirection: Axis.horizontal,
                        shrinkWrap: true,
                        itemCount: _speciality.length,
                        itemBuilder: (BuildContext context, int idx) {
                          return InkWell(
                            onTap: () => print(_speciality[idx]),
                            child: Container(
                              margin: EdgeInsets.all(10.0),
                              child: Chip(
                                padding: EdgeInsets.all(10.0),
                                label: Text(_speciality[idx]),
                              ),
                            ),
                          );
                        },
                      ),*/
              ),
              Container(
                  margin: EdgeInsets.only(right: 10),
                  height: queryData.size.height * 0.10,
                  child: Row(
                    textDirection: TextDirection.rtl,
                    mainAxisSize: MainAxisSize.max,
                    children: <Widget>[
                      IconButton(
                        icon: Icon(
                          Icons.add_circle,
                          size: 40,
                        ),
                        color: Colors.teal,
                        onPressed: () {
                          _specialityAddPressed(context);
                        },
                      ),
                      Text(
                        'Add speciality',
                        style: TextStyle(fontSize: 16),
                      ),
                    ],
                  )),
              /*Container(
                height: queryData.size.height * .10,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    Text(
                      'Speciality :',
                      style: TextStyle(fontSize: 18),
                    ),
                    allSpecializationModel != null ? DropdownButton<AllSpecializationModel>(
                      value: selectedSpecialization,
                      items: this
                          .allSpecializationModel
                          ?.map((AllSpecializationModel value) {
                        return new DropdownMenuItem<AllSpecializationModel>(
                          value: value,
                          child: new Text(
                            value.speciality,
                            style: TextStyle(fontSize: 18),
                          ),
                        );
                      })?.toList(),
                      hint: Text('Select speciality'),
                      onChanged: ((AllSpecializationModel newValue) {
                        setState(() {
                          selectedSpecialization = newValue;
                          print(selectedSpecialization.speciality);
                        });
                      }),
                    ):Center(child: CircularProgressIndicator(),),
                  ],
                ),
              ),*/
              Container(
                height: queryData.size.height * 0.21,
                child: RangeSliderItem(
                  title: 'Price :',
                  initialMinValue: minValue,
                  initialMaxValue: maxValue,
                  onMinValueChanged: (v) {
                    minValue = v;
                    //print('MinValue : $minValue');
                  },
                  onMaxValueChanged: (v) {
                    maxValue = v;
                    //print('MaxValue : $maxValue');
                  },
                ),
              ),
              Container(
                height: queryData.size.height * 0.35,
                child: Center(
                  child: TextField(
                    textAlign: TextAlign.center,
                    controller: _controller,
                    keyboardType: TextInputType.text,
                    decoration: InputDecoration(
                      hintText: 'Custom Search',
                      hintStyle: TextStyle(fontSize: 16),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(8),
                          borderSide: BorderSide(
                            width: 1,
                            style: BorderStyle.solid,
                          ),
                          gapPadding: 2.0),
                      filled: true,
                      contentPadding: EdgeInsets.all(16),
                    ),
                  ),
                ),
                margin: EdgeInsets.all(4.0),
                padding: EdgeInsets.all(2.0),
              ),
            ],
          ),
        ));
  }

  void _specialityAddPressed(BuildContext context) async {
    _sp = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => Speciality(checkedValues: _sp)));
  }
}
