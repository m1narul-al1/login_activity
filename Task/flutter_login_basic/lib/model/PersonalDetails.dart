import 'package:http/http.dart' as http;
import 'dart:convert';
import 'Personal.dart';

class PersonalDetails {
  final String status;
  final String faildReson;
  final Personal personal;

  PersonalDetails({this.status, this.faildReson, this.personal});

  factory PersonalDetails.fromJson(Map<String, dynamic> json) {
    print(json['PersonalDetails']);
    return PersonalDetails(
      status: json['Status'],
      faildReson: json['FaildReson'],
      personal: (json['PersonalDetails']!=null) ? Personal.fromJson(json['PersonalDetails']) : null,
    );
  }
}

Future<PersonalDetails> fetchPostPersonalDetails(String securityID) async {
  var uri = Uri.http(
      "cure-staging-api.azurewebsites.net",
      Uri.encodeFull("/api/account/GetAccountInfo"),
      {"SecurityID": securityID});
  print(uri);
  final response = await http.post(uri);
  //print(response.body);
  if (response.statusCode == 200) {
    // If server returns an OK response, parse the JSON
    return PersonalDetails.fromJson(json.decode(response.body));
  } else {
    // If that response was not OK, throw an error.
    throw Exception('Failed to load post');
  }
}
