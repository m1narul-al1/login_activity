
import 'package:http/http.dart' as http;
import 'dart:convert';

List<AllSpecializationModel> allSpecializationModelFromJson(String str) => new List<AllSpecializationModel>.from(json.decode(str).map((x) => AllSpecializationModel.fromJson(x)));

String allSpecializationModelToJson(List<AllSpecializationModel> data) => json.encode(new List<dynamic>.from(data.map((x) => x.toJson())));

class AllSpecializationModel {
  int id;
  String speciality;

  AllSpecializationModel({
    this.id,
    this.speciality,
  });

  factory AllSpecializationModel.fromJson(Map<String, dynamic> json) => new AllSpecializationModel(
    id: json["ID"],
    speciality: json["Speciality"],
  );

  Map<String, dynamic> toJson() => {
    "ID": id,
    "Speciality": speciality,
  };
}

class CommonDataReturn {
  final String status;
  final String faildReson;
  final String code;

  CommonDataReturn({this.status, this.faildReson, this.code});

  factory CommonDataReturn.fromJson(Map<String, dynamic> json) {
    return CommonDataReturn(
      status: json['Status'],
      faildReson: json['FaildReson'],
      code: json['Code'],
    );
  }
}


Future<dynamic> fetchSpecialization({String securityID}) async {
  print(securityID);
  var uri = Uri.http("cure-staging-api.azurewebsites.net",
      Uri.encodeFull("/api/Doctor/AllSpecialization"));
  print(uri);
  final response = await http.get(uri,
      headers: {'SecurityID': securityID});
  print(response.statusCode);
  print(response.toString());
  print(json.decode(response.body));

  if (response.statusCode == 200) {
    if((json.decode(response.body)) is List){
      final allSpecializationModel = allSpecializationModelFromJson(response.body);
      //print(allSpecializationModel);
      return allSpecializationModel;
    } else
      // If server returns an OK response, parse the JSON
      return CommonDataReturn.fromJson(json.decode(response.body));
  } else {
    // If that response was not OK, throw an error.
    throw Exception('Failed to load post');
  }
}