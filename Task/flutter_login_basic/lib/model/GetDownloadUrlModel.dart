class GetDownloadUrlModel {
  String status;
  String code;
  String faildReson;

  String securityID;

  String documentID;

  GetDownloadUrlModel(
      {this.status,
      this.code,
      this.faildReson,
      this.securityID,
      this.documentID});

  factory GetDownloadUrlModel.fromJson(Map<String, dynamic> json) =>
      new GetDownloadUrlModel(
        status: json["Status"],
        code: json["Code"],
        faildReson: json["FaildReson"],
      );

  Map<String, String> toHeader() => {
        "securityID": securityID,
      };

  Map<String, String> toJson() => {
        "DocumentID": documentID,
      };
}
