
class EditProfileStatus{
  final String status;
  final String code;
  final String faildReson;

  EditProfileStatus({this.status, this.code, this.faildReson});

  factory EditProfileStatus.fromJson(Map<String, dynamic> json) {
    print(json['Status']);
    print(json['FaildReson']);

    return EditProfileStatus(
      code: json['Code'],
      status: json['Status'],
      faildReson: json['FaildReson'],
    );
  }


}