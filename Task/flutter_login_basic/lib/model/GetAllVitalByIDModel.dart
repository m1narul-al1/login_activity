class GetAllVitalByIdModel {
  int id;
  String vitalName;
  String value;
  DateTime recordedOn;
  String securityID;
  String patientID;
  var vitalID;

  GetAllVitalByIdModel({
    this.id,
    this.vitalName,
    this.value,
    this.recordedOn,
    this.securityID,
    this.patientID,
    this.vitalID
  });

  factory GetAllVitalByIdModel.fromJson(Map<String, dynamic> json) => new GetAllVitalByIdModel(
    id: json["ID"],
    vitalName: json["VitalName"],
    value: json["Value"],
    recordedOn: DateTime.parse(json["RecordedOn"]),
  );

  Map<String, String> toHeader() => {
    "securityID": securityID,
    "PatientID" : patientID
  };
  Map<String, String> toJson() => {
    "VitalID": vitalID,
  };
}
