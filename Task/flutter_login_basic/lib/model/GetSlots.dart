// To parse this JSON data, do
//
//     final getDoctorSlotModel = getDoctorSlotModelFromJson(jsonString);

import 'dart:convert';
import 'package:http/http.dart' as http;


List<GetDoctorSlotModel> getDoctorSlotModelFromJson(String str) => new List<GetDoctorSlotModel>.from(json.decode(str).map((x) => GetDoctorSlotModel.fromJson(x)));

String getDoctorSlotModelToJson(List<GetDoctorSlotModel> data) => json.encode(new List<dynamic>.from(data.map((x) => x.toJson())));

class GetDoctorSlotModel {
  String workDate;
  int workshiftId;
  List<Slot> slots;

  GetDoctorSlotModel({
    this.workDate,
    this.workshiftId,
    this.slots,
  });

  factory GetDoctorSlotModel.fromJson(Map<String, dynamic> json) => new GetDoctorSlotModel(
    workDate: json["WorkDate"],
    workshiftId: json["WorkshiftID"],
    slots: new List<Slot>.from(json["Slots"].map((x) => Slot.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "WorkDate": workDate,
    "WorkshiftID": workshiftId,
    "Slots": new List<dynamic>.from(slots.map((x) => x.toJson())),
  };
}

class Slot {
  String time;
  bool available;

  Slot({
    this.time,
    this.available,
  });

  factory Slot.fromJson(Map<String, dynamic> json) => new Slot(
    time: json["Time"],
    available: json["Available"],
  );

  Map<String, dynamic> toJson() => {
    "Time": time,
    "Available": available,
  };
}
class CommonDataReturn {
  final String status;
  final String faildReson;
  final String code;

  CommonDataReturn({this.status, this.faildReson, this.code});

  factory CommonDataReturn.fromJson(Map<String, dynamic> json) {
    return CommonDataReturn(
      status: json['Status'],
      faildReson: json['FaildReson'],
      code: json['Code'],
    );
  }
}
Future<dynamic> fetchSlots({id, chamberId}) async {
var body=json.encode({"doctorID": id, "clinicID": chamberId,"dayCount":"7"});
Map<String,String> headers = {
  'Content-type' : 'application/json',

};
  var uri = Uri.http("cure-staging-api.azurewebsites.net",
      Uri.encodeFull("/api/doctor/GetDoctorSlot"));
  print(uri);
  final response = await http.post(uri,body: body,headers: headers);
  print(response.statusCode);
  //print(response.toString());
  print(json.decode(response.body));

  if (response.statusCode == 200) {
    if((json.decode(response.body)) is List){
      final getDoctorSlotModel = getDoctorSlotModelFromJson(response.body);
      //print(getDoctorSlotModel);
      return getDoctorSlotModel;
    } else
      // If server returns an OK response, parse the JSON
      return CommonDataReturn.fromJson(json.decode(response.body));
  } else {
    // If that response was not OK, throw an error.
    throw Exception('Failed to load post');
  }
}
