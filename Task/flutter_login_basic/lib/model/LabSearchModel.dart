// To parse this JSON data, do
//
//     final labSearchModel = labSearchModelFromJson(jsonString);

import 'dart:convert';
import 'package:http/http.dart' as http;

List<LabSearchModel> labSearchModelFromJson(String str) => new List<LabSearchModel>.from(json.decode(str).map((x) => LabSearchModel.fromJson(x)));

String labSearchModelToJson(List<LabSearchModel> data) => json.encode(new List<dynamic>.from(data.map((x) => x.toJson())));

class LabSearchModel {
  String id;
  String firstName;
  String lastName;
  String middleName;
  String fullName;
  String picture;
  String storeName;
  String addressLine1;
  String addressLine2;
  String addressLine3;
  String city;
  int cityId;

  LabSearchModel({
    this.id,
    this.firstName,
    this.lastName,
    this.middleName,
    this.fullName,
    this.picture,
    this.storeName,
    this.addressLine1,
    this.addressLine2,
    this.addressLine3,
    this.city,
    this.cityId,
  });

  factory LabSearchModel.fromJson(Map<String, dynamic> json) => new LabSearchModel(
    id: json["ID"],
    firstName: json["FirstName"],
    lastName: json["LastName"],
    middleName: json["MiddleName"],
    fullName: json["FullName"],
    picture: json["Picture"] == null ? null : json["Picture"],
    storeName: json["StoreName"],
    addressLine1: json["AddressLine1"],
    addressLine2: json["AddressLine2"],
    addressLine3: json["AddressLine3"],
    city: json["City"],
    cityId: json["CityID"] == null ? null : json["CityID"],
  );

  Map<String, dynamic> toJson() => {
    "ID": id,
    "FirstName": firstName,
    "LastName": lastName,
    "MiddleName": middleName,
    "FullName": fullName,
    "Picture": picture == null ? null : picture,
    "StoreName": storeName,
    "AddressLine1": addressLine1,
    "AddressLine2": addressLine2,
    "AddressLine3": addressLine3,
    "City": city,
    "CityID": cityId == null ? null : cityId,
  };
}
class CommonDataReturn {
  final String status;
  final String faildReson;
  final String code;

  CommonDataReturn({this.status, this.faildReson, this.code});

  factory CommonDataReturn.fromJson(Map<String, dynamic> json) {
    return CommonDataReturn(
      status: json['Status'],
      faildReson: json['FaildReson'],
      code: json['Code'],
    );
  }
}

Future<dynamic> fetchLabsSearch({Map returnedValue, String location}) async {
  var uri = Uri.http("cure-staging-api.azurewebsites.net",
      Uri.encodeFull("/api/search/LabSearch"));
  Map<String,String> headers = {
    'Content-type' : 'application/json',
    'Accept': 'application/json',
  };
  print(uri);
  print(returnedValue);
  var body = json.encode({
    "City": /*location != null ? location :*/ "",
    "CustomSearch": returnedValue != null
        ? returnedValue['customText'].isNotEmpty
        ? returnedValue['customText']
        : ""
        : ""
  });
  print(body);
  final response = await http.post(uri,body: body,headers: headers);
  //print(response.statusCode);
  print(response.body);
  //print(json.decode(response.body));

  if (response.statusCode == 200) {
    if ((json.decode(response.body)) is List) {
     final labSearchModel = labSearchModelFromJson(response.body);
      //print(labSearchModel);
      return labSearchModel;
    } else
      // If server returns an OK response, parse the JSON
      return CommonDataReturn.fromJson(json.decode(response.body));
  } else {
    // If that response was not OK, throw an error.
    throw Exception('Failed to load post');
  }
}
