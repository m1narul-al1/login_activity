import 'package:http/http.dart' as http;
import 'dart:convert';

class GetDistinctDays {
  final String status;
  final String faildReson;
  final String code;

  GetDistinctDays({this.status, this.faildReson, this.code});

  factory GetDistinctDays.fromJson(Map<String, dynamic> json) {
    return GetDistinctDays(
      status: json['Status'],
      faildReson: json['FaildReson'],
      code: json['Code'],
    );
  }
}

class DaysList {
  final String days;

  const DaysList({this.days});
}

Future<dynamic> fetchPostDays({String securityID}) async {
  var uri = Uri.http("cure-staging-api.azurewebsites.net",
      Uri.encodeFull("/api/doctor/GetDistinctDays"));
  print(uri);
  final response = await http.get(uri,
      headers:{"SecurityID": securityID});
  //print(response.body);
  if (response.statusCode == 200) {
    // If server returns an OK response, parse the JSON
    if (json.decode(response.body) is List) {
      List responseJson = json.decode(response.body);
      List<String> dayList = createDaysList(responseJson);
      //print(dayList is List<String>);
      return dayList;
    }
    if (json.decode(response.body) is Map) {
      return GetDistinctDays.fromJson(json.decode(response.body));
    }
  } else {
    // If that response was not OK, throw an error.
    throw Exception('Failed to load post');
  }
}

List<String> createDaysList(List responseJson) {
  //print(responseJson);
  List<String> list = new List();
  for(int i=0;i<responseJson.length;i++){
    //print(responseJson[i]);
    list.add(responseJson[i]);
  }
  //print(list is List<String>);
  return list;

}