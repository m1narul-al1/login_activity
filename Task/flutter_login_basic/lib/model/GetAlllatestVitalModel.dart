class GetAlllatestVitalModel {
  int vitalId;
  String vitalName;
  int lastUpdatedValue;
  DateTime lastUpdatedDate;
  String securityID;
  String patientID;

  GetAlllatestVitalModel({
    this.vitalId,
    this.vitalName,
    this.lastUpdatedValue,
    this.lastUpdatedDate,
    this.securityID,
    this.patientID
  });

  factory GetAlllatestVitalModel.fromJson(Map<String, dynamic> json) => new GetAlllatestVitalModel(
    vitalId: json["VitalID"],
    vitalName: json["VitalName"],
    lastUpdatedValue: json["LastUpdatedValue"],
    lastUpdatedDate: DateTime.parse(json["LastUpdatedDate"]),
  );

  Map<String, String> toHeader() => {
    "securityID": securityID,
    "PatientID" : patientID
  };
}
