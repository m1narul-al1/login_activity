class Personal {
  bool displayMobile;
  int userInfoStatus,stateID,countryID;
  String firstName,
      middleName,
      lastName,
      dOB,
      profilePicture,
      addressLine1,
      addressLine2,
      addressLine3,
      city,
      zipCode,
      homePhone,
      officePhone,
      mobile1,
      mobile2,
      aadharno,
      website,
      gender,
      state,
      country;

  Personal({this.userInfoStatus, this.firstName, this.middleName,
    this.lastName, this.dOB, this.profilePicture, this.addressLine1,
    this.addressLine2, this.addressLine3, this.city, this.zipCode,
    this.homePhone, this.officePhone, this.mobile1, this.mobile2,
    this.aadharno, this.website, this.displayMobile, this.gender, this.state,
    this.stateID, this.country, this.countryID});

  factory Personal.fromJson(Map<String, dynamic> json) {
    return Personal(
      userInfoStatus: json['UserInfoStatus'],
      firstName: json['FirstName'],
      middleName: json['MiddleName'],
      lastName: json['LastName'],
      dOB: json['DOB'],
      profilePicture: json['ProfilePicture'],
      addressLine1: json['AddressLine1'],
      addressLine2: json['AddressLine2'],
      addressLine3: json['AddressLine3'],
      city: json['City'],
      zipCode: json['ZipCode'],
      homePhone: json['HomePhone'],
      officePhone: json['OfficePhone'],
      mobile1: json['Mobile1'],
      mobile2: json['Mobile2'],
      aadharno: json['Aadharno'],
      website: json['Website'],
      displayMobile: json['DisplayMobile'],
      gender: json['Gender'],
      state: json['State'],
      stateID: json['StateID'],
      country: json['Country'],
      countryID: json['CountryID'],
    );
  }

}