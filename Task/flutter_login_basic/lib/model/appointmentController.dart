import 'package:http/http.dart' as http;
import 'dart:convert';

class AppointmentMap {
  final String code;
  final String status;
  final String failedReson;

  AppointmentMap({this.code, this.status, this.failedReson});

  factory AppointmentMap.fromJson(Map<String, dynamic> json) {
    return AppointmentMap(
      code: json['Code'],
      failedReson: json['FaildReson'],
      status: json['Status'],
    );
  }
}

class AppointmentList {
  final List list;

  AppointmentList([this.list]);

  factory AppointmentList.fromJson(Map<String, dynamic> json) {
    return AppointmentList();
  }
}

Future<AppointmentMap> fetchPostAppointment(String securityID) async {
  print(securityID);
  var body = json.encode({'SecurityID': securityID});
  var uri = Uri.http("cure-staging-api.azurewebsites.net",
      Uri.encodeFull("/api/Patient/GetAppointementDetails"));
  print(uri);
  final response = await http.post(uri,
      headers: {"Content-Type": "application/json"}, body: body);
  print(response.statusCode);
  print(response.toString());
  print(json.decode(response.body));

  if (response.statusCode == 200) {
    if (response is List) {
      return null;
    } else
      // If server returns an OK response, parse the JSON
      return AppointmentMap.fromJson(json.decode(response.body));
  } else {
    // If that response was not OK, throw an error.
    throw Exception('Failed to load post');
  }
}
