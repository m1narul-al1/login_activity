
import 'dart:convert';
import 'package:http/http.dart' as http;

List<DirectorySearchModel> directorySearchModelFromJson(String str) => new List<DirectorySearchModel>.from(json.decode(str).map((x) => DirectorySearchModel.fromJson(x)));

class DirectorySearchModel {
  String name;
  String profilePicture;
  String address;
  String phone;
  String securityID;

  DirectorySearchModel({
    this.name,
    this.profilePicture,
    this.address,
    this.phone,
    this.securityID
  });

  factory DirectorySearchModel.fromJson(Map<String, dynamic> json) => new DirectorySearchModel(
    name: json["name"] == null ? null : json["name"],
    profilePicture: json["profilePicture"] == null ? null : json["profilePicture"],
    address: json["address"] == null ? null : json["address"],
    phone: json["phone"] == null ? null : json["phone"],
  );

  Map<String, String> toHeader() => {
    "securityID": securityID,
  };
}

class CommonDataReturn {
  final String status;
  final String faildReson;
  final String code;

  CommonDataReturn({this.status, this.faildReson, this.code});

  factory CommonDataReturn.fromJson(Map<String, dynamic> json) {
    return CommonDataReturn(
      status: json['Status'],
      faildReson: json['FaildReson'],
      code: json['Code'],
    );
  }
}

Future<dynamic> getAllPatient({String securityID}) async {
  var uri = Uri.http("cure-staging-api.azurewebsites.net",
      Uri.encodeFull("/api/doctor/GetAllPatient"));
  print(uri);
  final response = await http.get(uri,
      headers: {"SecurityID": securityID},);
  print(json.decode(response.body)  is List);
  if (response.statusCode == 200) {
    // If server returns an OK response, parse the JSON

    if((json.decode(response.body)) is List){
      final directorySearchModel = directorySearchModelFromJson(response.body);
      //print(directorySearchModel);
      return directorySearchModel;
    }else{
      return CommonDataReturn.fromJson(json.decode(response.body));
    }
  } else {
    // If that response was not OK, throw an error.
    throw Exception('Failed to load post');
  }
}