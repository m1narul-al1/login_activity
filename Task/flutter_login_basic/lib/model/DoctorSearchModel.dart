// To parse this JSON data, do
//
//     final doctorSearchModel = doctorSearchModelFromJson(jsonString);
import 'package:http/http.dart' as http;
import 'dart:convert';

List<DoctorSearchModel> doctorSearchModelFromJson(String str) =>
    new List<DoctorSearchModel>.from(
        json.decode(str).map((x) => DoctorSearchModel.fromJson(x)));

String doctorSearchModelToJson(List<DoctorSearchModel> data) =>
    json.encode(new List<dynamic>.from(data.map((x) => x.toJson())));

class DoctorSearchModel {
  String firstName;
  String lastName;
  String middleName;
  String fullName;
  String chamberName;
  String addressLine1;
  int experience;
  double fees;
  dynamic picture;
  String specialization;
  int specialityId;
  String tagline;
  String id;
  String city;
  String otherSpecialization;
  bool verified;
  int chamberId;

  DoctorSearchModel({
    this.firstName,
    this.lastName,
    this.middleName,
    this.fullName,
    this.chamberName,
    this.addressLine1,
    this.experience,
    this.fees,
    this.picture,
    this.specialization,
    this.specialityId,
    this.tagline,
    this.id,
    this.city,
    this.otherSpecialization,
    this.verified,
    this.chamberId,
  });

  factory DoctorSearchModel.fromJson(Map<String, dynamic> json) =>
      new DoctorSearchModel(
        firstName: json["FirstName"] == null ? null : json["FirstName"],
        lastName: json["LastName"] == null ? null : json["LastName"],
        middleName: json["MiddleName"] == null ? null : json["MiddleName"],
        fullName: json["FullName"] == null ? null : json["FullName"],
        chamberName: json["ChamberName"] == null ? null : json["ChamberName"],
        addressLine1:
            json["AddressLine1"] == null ? null : json["AddressLine1"],
        experience: json["Experience"] == null ? null : json["Experience"],
        fees: json["Fees"] == null ? null : json["Fees"],
        picture: json["Picture"],
        specialization:
            json["Specialization"] == null ? null : json["Specialization"],
        specialityId:
            json["SpecialityID"] == null ? null : json["SpecialityID"],
        tagline: json["Tagline"] == null ? null : json["Tagline"],
        id: json["ID"] == null ? null : json["ID"],
        city: json["City"] == null ? null : json["City"],
        otherSpecialization: json["OtherSpecialization"] == null
            ? null
            : json["OtherSpecialization"],
        verified: json["Verified"] == null ? null : json["Verified"],
        chamberId: json["ChamberID"] == null ? null : json["ChamberID"],
      );

  Map<String, dynamic> toJson() => {
        "FirstName": firstName == null ? null : firstName,
        "LastName": lastName == null ? null : lastName,
        "MiddleName": middleName == null ? null : middleName,
        "FullName": fullName == null ? null : fullName,
        "ChamberName": chamberName == null ? null : chamberName,
        "AddressLine1": addressLine1 == null ? null : addressLine1,
        "Experience": experience == null ? null : experience,
        "Fees": fees == null ? null : fees,
        "Picture": picture,
        "Specialization": specialization == null ? null : specialization,
        "SpecialityID": specialityId == null ? null : specialityId,
        "Tagline": tagline == null ? null : tagline,
        "ID": id == null ? null : id,
        "City": city == null ? null : city,
        "OtherSpecialization":
            otherSpecialization == null ? null : otherSpecialization,
        "Verified": verified == null ? null : verified,
        "ChamberID": chamberId == null ? null : chamberId,
      };
}

class CommonDataReturn {
  final String status;
  final String faildReson;
  final String code;

  CommonDataReturn({this.status, this.faildReson, this.code});

  factory CommonDataReturn.fromJson(Map<String, dynamic> json) {
    return CommonDataReturn(
      status: json['Status'],
      faildReson: json['FaildReson'],
      code: json['Code'],
    );
  }
}

Future<dynamic> fetchDoctor({Map returnedValue, String location}) async {
  var uri = Uri.http("cure-staging-api.azurewebsites.net",
      Uri.encodeFull("/api/search/DoctorSearch"));
  Map<String,String> headers = {
    'Content-type' : 'application/json',
    'Accept': 'application/json',
  };
  print(uri);
  print(returnedValue);
  var body = json.encode({
    "City": /*location != null ? location :*/ "",
    "Speciality": returnedValue != null
        ? returnedValue['spvalue'] != null ? returnedValue['spvalue'] : ""
        : "",
    "FressFrom": returnedValue != null
        ? returnedValue['minValue'] != null
        ? returnedValue['minValue'].toString()
        : 0.toString()
        : 0.toString(),
    "FreesTo": returnedValue != null
        ? returnedValue['maxValue'] != null
        ? returnedValue['maxValue'].toString()
        : 1000.toString()
        : 1000.toString(),
    "CustomSearch": returnedValue != null
        ? returnedValue['customText'].isNotEmpty
        ? returnedValue['customText']
        : ""
        : ""
  });
  print(body);
  final response = await http.post(uri,body: body,headers: headers);
  //print(response.statusCode);
  print(response.body);
  //print(json.decode(response.body));

  if (response.statusCode == 200) {
    if ((json.decode(response.body)) is List) {
      final doctorSearchModel = doctorSearchModelFromJson(response.body);
      //print(doctorSearchModel);
      return doctorSearchModel;
    } else
      // If server returns an OK response, parse the JSON
      return CommonDataReturn.fromJson(json.decode(response.body));
  } else {
    // If that response was not OK, throw an error.
    throw Exception('Failed to load post');
  }
}
