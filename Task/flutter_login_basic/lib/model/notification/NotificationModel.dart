// To parse this JSON data, do
//
//     final notificationModel = notificationModelFromJson(jsonString);

import 'dart:convert';

List<NotificationModel> notificationModelFromJson(String str) =>
    new List<NotificationModel>.from(
        json.decode(str).map((x) => NotificationModel.fromJson(x)));

String notificationModelToJson(List<NotificationModel> data) =>
    json.encode(new List<dynamic>.from(data.map((x) => x.toHeader())));

class NotificationModel {
  int notificationId;
  String senderId;
  String sender;
  String receiverId;
  String receiver;
  String message;
  String status;
  String type;
  String title;
  bool isRead;
  int notificationType;
  bool isRoot;
  String securityID;

  NotificationModel(
      {this.notificationId,
      this.senderId,
      this.sender,
      this.receiverId,
      this.receiver,
      this.message,
      this.status,
      this.type,
      this.title,
      this.isRead,
      this.notificationType,
      this.isRoot,
      this.securityID});

  factory NotificationModel.fromJson(Map<String, dynamic> json) =>
      new NotificationModel(
        notificationId:
            json["NotificationID"] == null ? null : json["NotificationID"],
        senderId: json["SenderID"] == null ? null : json["SenderID"],
        sender: json["Sender"] == null ? null : json["Sender"],
        receiverId: json["ReceiverID"] == null ? null : json["ReceiverID"],
        receiver: json["Receiver"] == null ? null : json["Receiver"],
        message: json["Message"] == null ? null : json["Message"],
        status: json["Status"] == null ? null : json["Status"],
        type: json["Type"] == null ? null : json["Type"],
        title: json["Title"] == null ? null : json["Title"],
        isRead: json["IsRead"] == null ? null : json["IsRead"],
        notificationType:
            json["NotificationType"] == null ? null : json["NotificationType"],
        isRoot: json["IsRoot"] == null ? null : json["IsRoot"],
      );

  Map<String, dynamic> toHeader() => {
        "SecurityID": securityID == null ? null : securityID,
      };
}
