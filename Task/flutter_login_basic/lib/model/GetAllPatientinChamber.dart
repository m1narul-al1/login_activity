import 'dart:convert';
class GetAllPatientinChamber{
  List<PatientList> _list;
  String currentTime;
  String clinicId;
  String securityID;

  GetAllPatientinChamber(this._list, this.currentTime, this.clinicId,
      this.securityID);
  List<PatientList> getAllPatientinChamberFromJson(String str) => new List<PatientList>.from(json.decode(str).map((x) => PatientList.fromJson(x)));

  Map<String, String> toJson() => {
    "currentTime": currentTime,
    "clinicID": clinicId,
  };
  Map<String, String> toHeader() => {
    "securityID": securityID,
  };
}
class PatientList {
  String uniqueId;
  String firstName;
  String lastName;
  String gender;
  String patientProfilePicture;
  String contactNo;
  String dOb;


  PatientList({
    this.uniqueId,
    this.firstName,
    this.lastName,
    this.gender,
    this.patientProfilePicture,
    this.contactNo,
    this.dOb
  });

  factory PatientList.fromJson(Map<String, dynamic> json) => new PatientList(
    uniqueId: json["uniqueID"],
    firstName: json["firstName"],
    lastName: json["lastName"],
    gender: json["gender"],
    patientProfilePicture: json["patientProfilePicture"],
    contactNo: json["contactNo"],
    dOb: json["dOB"],
  );

}
