class GetAllAppointmetByIdModel {
  List<DocAppointment> docAppointment;
  List<LabAppointment> labAppointment;
  String securityID;
  String memberID;

  GetAllAppointmetByIdModel({
    this.docAppointment,
    this.labAppointment,
    this.securityID,
    this.memberID
  });

  factory GetAllAppointmetByIdModel.fromJson(Map<String, dynamic> json) => new GetAllAppointmetByIdModel(
    docAppointment: new List<DocAppointment>.from(json["docAppointment"].map((x) => DocAppointment.fromJson(x))),
    labAppointment: new List<LabAppointment>.from(json["labAppointment"].map((x) => LabAppointment.fromJson(x))),
  );
  Map<String, String> toHeader() => {
    "securityID": securityID,
    "MemberID" : memberID
  };
}

class DocAppointment {
  int id;
  String doctorName;
  String doctorMobile;
  DateTime appointmentTime;
  bool confirmed;
  DateTime bookingDate;
  String bookedBy;

  DocAppointment({
    this.id,
    this.doctorName,
    this.doctorMobile,
    this.appointmentTime,
    this.confirmed,
    this.bookingDate,
    this.bookedBy,
  });

  factory DocAppointment.fromJson(Map<String, dynamic> json) => new DocAppointment(
    id: json["ID"],
    doctorName: json["DoctorName"],
    doctorMobile: json["DoctorMobile"],
    appointmentTime: DateTime.parse(json["AppointmentTime"]),
    confirmed: json["Confirmed"],
    bookingDate: DateTime.parse(json["BookingDate"]),
    bookedBy: json["BookedBy"],
  );

  Map<String, dynamic> toJson() => {
    "ID": id,
    "DoctorName": doctorName,
    "DoctorMobile": doctorMobile,
    "AppointmentTime": appointmentTime.toIso8601String(),
    "Confirmed": confirmed,
    "BookingDate": bookingDate.toIso8601String(),
    "BookedBy": bookedBy,
  };
}

class LabAppointment {
  int id;
  String labName;
  String labMobile;
  DateTime appointmentTime;
  bool confirmed;
  DateTime bookingDate;
  String bookedBy;

  LabAppointment({
    this.id,
    this.labName,
    this.labMobile,
    this.appointmentTime,
    this.confirmed,
    this.bookingDate,
    this.bookedBy,
  });

  factory LabAppointment.fromJson(Map<String, dynamic> json) => new LabAppointment(
    id: json["ID"],
    labName: json["LabName"],
    labMobile: json["LabMobile"],
    appointmentTime: DateTime.parse(json["AppointmentTime"]),
    confirmed: json["Confirmed"],
    bookingDate: DateTime.parse(json["BookingDate"]),
    bookedBy: json["BookedBy"],
  );
}
