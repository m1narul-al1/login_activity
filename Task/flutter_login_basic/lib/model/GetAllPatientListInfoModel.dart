class GetAllPatientListInfoModel {
  String uniqueId;
  String firstName;
  String lastName;
  String patientProfilePicture;
  String contactNo;
  String dOb;
  String clinicName;
  String clinicId;
  String currentTime;
  String securityID;

  GetAllPatientListInfoModel(
      {this.uniqueId,
      this.firstName,
      this.lastName,
      this.patientProfilePicture,
      this.contactNo,
      this.dOb,
      this.clinicName,
      this.clinicId,
      this.currentTime,
      this.securityID});

  factory GetAllPatientListInfoModel.fromJson(Map<String, dynamic> json) =>
      new GetAllPatientListInfoModel(
        uniqueId: json["uniqueID"],
        firstName: json["firstName"],
        lastName: json["lastName"],
        patientProfilePicture: json["patientProfilePicture"],
        contactNo: json["contactNo"],
        dOb: json["dOB"],
        clinicName: json["ClinicName"],
        clinicId: json["clinicID"],
      );

  Map<String, String> toJson() => {
        "currentTime": currentTime,
      };

  Map<String, String> toHeader() => {
        "securityID": securityID,
      };
}
