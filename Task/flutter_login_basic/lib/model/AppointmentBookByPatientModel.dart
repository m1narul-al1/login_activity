// To parse this JSON data, do
//
//     final appointmentBookByPatientModel = appointmentBookByPatientModelFromJson(jsonString);

import 'dart:convert';
import 'package:http/http.dart' as http;

AppointmentBookByPatientModel appointmentBookByPatientModelFromJson(
        String str) =>
    AppointmentBookByPatientModel.fromJson(json.decode(str));

String appointmentBookByPatientModelToJson(
        AppointmentBookByPatientModel data) =>
    json.encode(data.toJson());

class AppointmentBookByPatientModel {
  int workshiftId;
  String workDate;
  String time;
  String status;
  String code;
  String faildReson;

  AppointmentBookByPatientModel(
      {this.workshiftId,
      this.workDate,
      this.time,
      this.status,
      this.faildReson,
      this.code});

  factory AppointmentBookByPatientModel.fromJson(Map<String, dynamic> json) =>
      new AppointmentBookByPatientModel(
        status: json["Status"],
        code: json["Code"],
        faildReson: json["FaildReson"],
      );

  Map<String, dynamic> toJson() => {
        "WorkshiftID": workshiftId,
        "WorkDate": workDate,
        "Time": time,
      };
}

Future<dynamic> bookSlot(
    {String workDate,
    int workshiftId,
    String time,
    String securityID,
    String memberID}) async {
  print('WorkDate --> $workDate WorkshiftID --> $workshiftId Time --> $time');
  print('securityID --> $securityID memberID --> $memberID');
  Map<String, String> headers;
  var body = json
      .encode({"WorkshiftID": workshiftId, "WorkDate": workDate, "Time": time});
  if (memberID == null) {
    headers = {
      'SecurityID': securityID,
      'Content-type': 'application/json',
    };
  } else {
    headers = {
      'SecurityID': securityID,
      'memberID': memberID,
      'Content-type': 'application/json',
    };
  }
  var uri = Uri.http("cure-staging-api.azurewebsites.net",
      Uri.encodeFull("/api/patient/AppointmentBookByPatient"));
  print(uri);
  final response = await http.post(uri, body: body, headers: headers);
  print(response.statusCode);
  //print(response.toString());
  print(json.decode(response.body));

  if (response.statusCode == 200) {
    final appointmentBookByPatientModel =
        appointmentBookByPatientModelFromJson(response.body);
    //print(getDoctorSlotModel);
    return appointmentBookByPatientModel;
  } else {
    // If that response was not OK, throw an error.
    throw Exception('Failed to load post');
  }
}
