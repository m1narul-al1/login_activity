
class ChangePasswordModel{
  final String status;
  final String newSecurityID;
  final String faildReson;

  ChangePasswordModel({this.status, this.newSecurityID, this.faildReson});

  factory ChangePasswordModel.fromJson(Map<String, dynamic> json) {
    if(json['Status']== "Success") {
      return ChangePasswordModel(
        newSecurityID: json['NewSecurityID'],
        status: json['Status'],
        faildReson: json['FaildReson'],
      );
    }else{
      return ChangePasswordModel(
        newSecurityID: json['Code'],
        status: json['Status'],
        faildReson: json['FaildReson'],
      );
    }
  }
}