class UpdatePendingListModel {
  List<PendingList> pendingList;

  String securityID;
  var uniqueID;

  UpdatePendingListModel({
    this.pendingList,
    this.securityID,
    this.uniqueID
  });

  factory UpdatePendingListModel.fromJson(Map<String, dynamic> json) => new UpdatePendingListModel(
    pendingList: new List<PendingList>.from(json["PendingList"].map((x) => PendingList.fromJson(x))),
  );

  Map<String, String> toHeader() => {
    "securityID": securityID,
  };
  Map<String, String> toJson() => {
    "uniqueID": uniqueID,
  };
}

class PendingList {
  String uniqueId;
  String firstName;
  String lastName;
  dynamic profilePicture;
  String contactNo;
  DateTime dOb;
  String status;
  DateTime timings;
  String clinicName;

  PendingList({
    this.uniqueId,
    this.firstName,
    this.lastName,
    this.profilePicture,
    this.contactNo,
    this.dOb,
    this.status,
    this.timings,
    this.clinicName,
  });

  factory PendingList.fromJson(Map<String, dynamic> json) => new PendingList(
    uniqueId: json["uniqueID"],
    firstName: json["firstName"],
    lastName: json["lastName"],
    profilePicture: json["profilePicture"],
    contactNo: json["contactNo"],
    dOb: DateTime.parse(json["dOB"]),
    status: json["status"],
    timings: DateTime.parse(json["timings"]),
    clinicName: json["clinicName"],
  );
}
