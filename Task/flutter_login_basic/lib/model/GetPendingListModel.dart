import 'package:http/http.dart' as http;
import 'dart:convert';
class GetPendingListModel {
  List<PendingList> pendingList;
  String uniqueId;
  bool status;
  String securityID;
  GetPendingListModel({
    this.pendingList,
    this.uniqueId,
    this.status,
    this.securityID
  });

  factory GetPendingListModel.fromJson(Map<String, dynamic> json) => new GetPendingListModel(
    pendingList: new List<PendingList>.from(json["PendingList"].map((x) => PendingList.fromJson(x))),
  );
  Map<String, dynamic> toJson() => {
    "uniqueID": uniqueId,
    "status": status,
  };

  Map<String, String> toHeader() => {
    "securityID": securityID,
  };
}

class PendingList {
  String uniqueId;
  String firstName;
  String lastName;
  String profilePicture;
  String contactNo;
  String dOb;
  String status;
  String timings;
  String clinicName;

  PendingList({
    this.uniqueId,
    this.firstName,
    this.lastName,
    this.profilePicture,
    this.contactNo,
    this.dOb,
    this.status,
    this.timings,
    this.clinicName,
  });

  factory PendingList.fromJson(Map<String, dynamic> json) => new PendingList(
    uniqueId: json["uniqueID"],
    firstName: json["firstName"],
    lastName: json["lastName"],
    profilePicture: json["profilePicture"] == null ? null : json["profilePicture"],
    contactNo: json["contactNo"],
    dOb: json["dOB"],
    status: json["status"],
    timings: json["timings"],
    clinicName: json["clinicName"],
  );
}
Future<dynamic> fetchGetPendingList(String sId) async {
  var uri = Uri.http("cure-staging-api.azurewebsites.net",
      Uri.encodeFull("/api/Doctor/GetPendingList"));
  print(uri);
  var body = {"SecurityID": 'jf9KeTQULfYVOxgIByuNgA'};
  final response = await http.get(uri, headers: body);
  print(response.body);
  print(json.decode(response.body)["PendingList"]!=null);
  if (response.statusCode == 200) {
    // If server returns an OK response, parse the JSON
    if(json.decode(response.body)["PendingList"]!=null){
      List responseJson = json.decode(response.body)["PendingList"];
      List<PendingList> pendingList = createPendingList(responseJson);
      return pendingList;
    }
    return GetPendingListModel.fromJson(json.decode(response.body));
  } else {
    // If that response was not OK, throw an error.
    throw Exception('Failed to load post');
  }
}

List<PendingList> createPendingList(List json) {
  print(json);
  List<PendingList> list = new List();
  for (int i = 0; i < json.length; i++) {
    String uniqueId = json[i]["uniqueID"];
    String firstName = json[i]["firstName"];
    String lastName = json[i]["lastName"];
    String profilePicture = json[i]["profilePicture"] == null ? null : json[i]["profilePicture"];
    String contactNo = json[i]["contactNo"];
    String dOb = json[i]["dOB"];
    String status = json[i]["status"];
    String timings = json[i]["timings"];
    String clinicName = json[i]["clinicName"];
    PendingList model = new PendingList(clinicName: clinicName,contactNo: contactNo,dOb: dOb,firstName: firstName,lastName: lastName,profilePicture: profilePicture,status: status,timings: timings,uniqueId: uniqueId);
    list.add(model);
  }
  return list;
}