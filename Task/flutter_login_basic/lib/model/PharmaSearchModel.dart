// To parse this JSON data, do
//
//     final pharmaSearchModel = pharmaSearchModelFromJson(jsonString);
import 'package:http/http.dart' as http;
import 'dart:convert';

List<Map<String, String>> pharmaSearchModelFromJson(String str) =>
    new List<Map<String, String>>.from(json.decode(str).map((x) =>
        new Map.from(x).map(
            (k, v) => new MapEntry<String, String>(k, v == null ? null : v))));

String pharmaSearchModelToJson(List<Map<String, String>> data) =>
    json.encode(new List<dynamic>.from(data.map((x) => new Map.from(x).map(
        (k, v) => new MapEntry<String, dynamic>(k, v == null ? null : v)))));
class CommonDataReturn {
  final String status;
  final String faildReson;
  final String code;

  CommonDataReturn({this.status, this.faildReson, this.code});

  factory CommonDataReturn.fromJson(Map<String, dynamic> json) {
    return CommonDataReturn(
      status: json['Status'],
      faildReson: json['FaildReson'],
      code: json['Code'],
    );
  }
}
Future<dynamic> fetchPharmaSearch({Map returnedValue, String location}) async {
  var uri = Uri.http("cure-staging-api.azurewebsites.net",
      Uri.encodeFull("/api/search/PharmaSearch"));
  Map<String,String> headers = {
    'Content-type' : 'application/json',
    'Accept': 'application/json',
  };
  print(uri);
  print(returnedValue);
  var body = json.encode({
    "City": /*location != null ? location :*/ "",
    "CustomSearch": returnedValue != null
        ? returnedValue['customText'].isNotEmpty
        ? returnedValue['customText']
        : ""
        : ""
  });
  print(body);
  final response = await http.post(uri,body: body,headers: headers);
  //print(response.statusCode);
  print(response.body);
  //print(json.decode(response.body));

  if (response.statusCode == 200) {
    if ((json.decode(response.body)) is List) {
      final pharmaSearchModel = pharmaSearchModelFromJson(response.body);
      //print(pharmaSearchModel);
      return pharmaSearchModel;
    } else
      // If server returns an OK response, parse the JSON
      return CommonDataReturn.fromJson(json.decode(response.body));
  } else {
    // If that response was not OK, throw an error.
    throw Exception('Failed to load post');
  }
}
