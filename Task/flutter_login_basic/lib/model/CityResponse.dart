import 'package:http/http.dart' as http;
import 'dart:convert';

class CityResponse{
  final String status;
  final String faildReson;
  final String code;
  CityResponse({this.status, this.faildReson, this.code});
  factory CityResponse.fromJson(Map<String, dynamic> json) {
    return CityResponse(
      status: json['Status'],
      faildReson: json['FaildReson'],
      code: json['Code'],
    );
  }


}
class CityList{
  final int id;
  final String cityName;
  final int stateID;
  final String stateName;

  const CityList({this.id, this.cityName, this.stateID, this.stateName});
}

Future<dynamic> fetchPostCity() async {
  var uri = Uri.http("cure-staging-api.azurewebsites.net", Uri.encodeFull("/api/account/GetAllCity"));
  print(uri);
  final response =
  await http.post(uri);
  //print(response.body);
  if (response.statusCode == 200) {
    // If server returns an OK response, parse the JSON
    if(json.decode(response.body) is List){
      List responseJson = json.decode(response.body);
      List<CityList> cityList = createCityList(responseJson);
      //print(stateList);
      return cityList;
    }
    if(json.decode(response.body) is Map){
      return CityResponse.fromJson(json.decode(response.body));
    }
  }else {
    // If that response was not OK, throw an error.
    throw Exception('Failed to load post');
  }
}

List<CityList> createCityList(List responseJson) {
  List<CityList> list = new List();
  for (int i = 0; i < responseJson.length; i++) {
    int id = responseJson[i]["ID"];
    String cityName = responseJson[i]["CityName"];
    int stateID = responseJson[i]["StateID"];
    String stateName = responseJson[i]["StateName"];
    CityList model = new CityList(cityName: cityName,id: id,stateID: stateID,stateName: stateName );
    list.add(model);
  }
  return list;
}