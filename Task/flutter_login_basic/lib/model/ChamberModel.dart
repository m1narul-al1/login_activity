import 'package:http/http.dart' as http;
import 'dart:convert';

class ChamberResponse {
  final String status;
  final String faildReson;
  final String code;

  ChamberResponse({this.status, this.faildReson, this.code});

  factory ChamberResponse.fromJson(Map<String, dynamic> json) {
    return ChamberResponse(
      status: json['Status'],
      faildReson: json['FaildReson'],
      code: json['Code'],
    );
  }
}

class ChamberModel {
  int chamberId;
  String chamberName;
  String phoneNumber;
  String country;
  String state;
  String city;
  String address;
  String zipCode;
  bool active;
  bool primary;

  ChamberModel({
    this.chamberId,
    this.chamberName,
    this.phoneNumber,
    this.country,
    this.state,
    this.city,
    this.address,
    this.zipCode,
    this.active,
    this.primary,
  });

  factory ChamberModel.fromJson(Map<String, dynamic> json) => new ChamberModel(
    chamberId: json["chamberID"],
    chamberName: json["chamberName"],
    phoneNumber: json["phoneNumber"],
    country: json["country"],
    state: json["state"],
    city: json["city"],
    address: json["address"],
    zipCode: json["zipCode"],
    active: json["active"],
    primary: json["primary"],
  );


}
Future<dynamic> fetchPostChamber(String sId) async {
  var uri = Uri.http("cure-staging-api.azurewebsites.net",
      Uri.encodeFull("/api/Doctor/GetAllChamber"));
  print(uri);
  var body = {"SecurityID": sId};
  final response = await http.get(uri, headers: body);
  //print(response.body);
  if (response.statusCode == 200) {
    // If server returns an OK response, parse the JSON
    if (json.decode(response.body) is List) {
      List responseJson = json.decode(response.body);
      List<ChamberModel> chamberList = createChamberList(responseJson);
      return chamberList;
    }
    if (json.decode(response.body) is Map) {
      return ChamberResponse.fromJson(json.decode(response.body));
    }
  } else {
    // If that response was not OK, throw an error.
    throw Exception('Failed to load post');
  }
}

List<ChamberModel> createChamberList(List responseJson) {
  print(responseJson);
  List<ChamberModel> list = new List();
  for (int i = 0; i < responseJson.length; i++) {
    int chamberId = responseJson[i]["chamberID"];
    String chamberName = responseJson[i]["chamberName"];
    String phoneNumber = responseJson[i]["phoneNumber"];
    String country = responseJson[i]["country"];
    String state = responseJson[i]["state"];
    String city = responseJson[i]["city"];
    String address = responseJson[i]["address"];
    String zipCode = responseJson[i]["zipCode"];
    bool active = responseJson[i]["active"];
    bool primary = responseJson[i]["primary"];
    ChamberModel model = new ChamberModel(
        active: active,
        address: address,
        chamberId: chamberId,
        chamberName: chamberName,
        city: city,
        country: country,
        phoneNumber: phoneNumber,
        primary: primary,
        state: state,
        zipCode: zipCode);
    list.add(model);
  }
  return list;
}
