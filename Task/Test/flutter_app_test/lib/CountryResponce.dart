
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class CountryResponse{
  final String Status;
  final String FaildReson;
  final String code;
  CountryResponse({this.Status, this.FaildReson, this.code});
  factory CountryResponse.fromJson(Map<String, dynamic> json) {
    return CountryResponse(
      Status: json['Status'],
      FaildReson: json['FaildReson'],
      code: json['Code'],
    );
  }

}

class CountryListModel{

  final int id;
  final String countryName;

  const CountryListModel({this.id, this.countryName});

}

Future<dynamic> fetchPostCountry() async {
  var uri = Uri.http("cure-staging-api.azurewebsites.net", Uri.encodeFull("/api/account/GetAllCountry"));
  print(uri);
  final response =
  await http.post(uri);
  //print(response.body);
  if (response.statusCode == 200) {
    // If server returns an OK response, parse the JSON
    if(json.decode(response.body) is List){
      List responseJson = json.decode(response.body);
      List<CountryListModel> countryList = createCountryList(responseJson);
      return countryList;
    }
    if(json.decode(response.body) is Map){
      return CountryResponse.fromJson(json.decode(response.body));
    }
  }else {
    // If that response was not OK, throw an error.
    throw Exception('Failed to load post');
  }
}

List<CountryListModel> createCountryList(List responseJson) {
  List<CountryListModel> list = new List();
  //List<DropdownMenuItem<String>> dropList =new List();
  for (int i = 0; i < responseJson.length; i++) {
    int id = responseJson[i]["ID"];
    String countryName = responseJson[i]["CountryName"];
    CountryListModel model = CountryListModel(id: id,countryName: countryName);
    list.add(model);
  }
  //dropList=list.map((CountryListModel value) {return new DropdownMenuItem<String>(value: value.countryName,child: new Text(value.countryName),);}).toList();
  return list;
}