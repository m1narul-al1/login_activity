import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class StateResponse{
  final String Status;
  final String FaildReson;
  final String code;
  StateResponse({this.Status, this.FaildReson, this.code});
  factory StateResponse.fromJson(Map<String, dynamic> json) {
    return StateResponse(
      Status: json['Status'],
      FaildReson: json['FaildReson'],
      code: json['Code'],
    );
  }


}
class StateListModel{
  final int id;
  final String stateName;
  final int countryID;
  final String countryName;

  const StateListModel({this.id, this.stateName, this.countryID, this.countryName});
}

Future<dynamic> fetchPostState() async {
  var uri = Uri.http("cure-staging-api.azurewebsites.net", Uri.encodeFull("/api/account/GetAllState"));
  print(uri);
  final response =
  await http.post(uri);
  //print(response.body);
  if (response.statusCode == 200) {
    // If server returns an OK response, parse the JSON
    if(json.decode(response.body) is List){
      List responseJson = json.decode(response.body);
      List<StateListModel> stateList = createStateList(responseJson);
      //print(stateList);
      return stateList;
    }
    if(json.decode(response.body) is Map){
      return StateResponse.fromJson(json.decode(response.body));
    }
  }else {
    // If that response was not OK, throw an error.
    throw Exception('Failed to load post');
  }
}

List<StateListModel> createStateList(List responseJson) {
  List<StateListModel> list = new List();
  List<DropdownMenuItem<String>> dropList =new List();
  for (int i = 0; i < responseJson.length; i++) {
    int id = responseJson[i]["ID"];
    String stateName = responseJson[i]["StateName"];
    int countryID = responseJson[i]["CountryID"];
    String countryName = responseJson[i]["CountryName"];
    StateListModel model = new StateListModel(countryID: countryID,countryName: countryName,id: id,stateName: stateName );
    list.add(model);
  }
  /*dropList=list.map((StateListModel value) {
    return new DropdownMenuItem<String>(
      value: value.stateName,
      child: new Text(value.stateName),
    );
  }).toList();*/
  return list;
}